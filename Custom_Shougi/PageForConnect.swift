//
//  PageForConnect.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

var isConnecting: Bool = false  //streamがつながっているか(streamを閉じたいときはwantToCloseを触る)
var inputStream : InputStream!
var outputStream: OutputStream!
var opponentName: String = ""
var opponentRate: String = ""
var is1P = false
var isSrv = false
var listenning = false
var isReceiving = true  //落ちないようにサーバから定期的に"xxx"という文字列を受信するようにしており、それが届いている、つまり通信できていることを確認するためのフラグ(read時にtrueにして数秒ごとにfalseにしている)
var isObserving = false //上記の監視用スレッドが存在しているか
var wantToClose = false //readと同期してソケットを閉じるため、streamを直接触らずにこのフラグを立てるだけ
var connectState = 0	//listenでの動作を決めるための状態管理(0が最初、1がマッチング後、2が駒情報受け取り後)

class PageForConnect: UIViewController,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //UITextFieldDelegateは今だけ
    let configs = UserDefaults.standard //保存データ
    let returnButton = UIButton()   //戻るボタン
    var list1 = UIButton()  //対戦相手選択ボタン
    var list2 = UIButton()  //対戦相手選択ボタン
    var list3 = UIButton()  //対戦相手選択ボタン
    var list4 = UIButton()  //対戦相手選択ボタン
    let receiveButton = UIButton()    //1Pになる(受付側になる)
    let titleLabel = UILabel()  //タイトルラベル
    let userDefaults = UserDefaults.standard
    var isPageSift = false
    var matchList = ["接続中"]  //相手の名前リスト
    var rateList = ["0"]  //相手のレートリスト
    var keyList = ["0"]  //相手のkey
    var matchTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        // Status Barの高さを取得する.
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        
        // TableViewの生成する(status barの高さ分ずらして表示).
        matchTableView = UITableView(frame: CGRect(x: 0, y:(screenHeight * 2/10) + barHeight, width: screenWidth, height: screenHeight * 5/10))
        // Cell名の登録をおこなう.
        matchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        // DataSourceの設定をする.
        matchTableView.dataSource = self
        // Delegateを設定する.
        matchTableView.delegate = self
        // Viewに追加する.
        self.view.addSubview(matchTableView)
        
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/8, y:screenHeight * 9/10, width:screenWidth/4, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        view.addSubview(returnButton)
        
        //1Pになるボタン
        receiveButton.frame = CGRect(x:screenWidth * 5/10, y:screenHeight * 1/10, width:screenWidth * 4/10, height: screenHeight * 2/40)
        receiveButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        receiveButton.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.8, alpha: 1)
        receiveButton.layer.borderWidth = 1.0
        receiveButton.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.8, alpha: 1).cgColor
        receiveButton.layer.cornerRadius = 25
        receiveButton.layer.shadowOpacity = 0.5
        receiveButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        receiveButton.setTitle("対戦を受け付ける", for: .normal)
        receiveButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        receiveButton.addTarget(self, action: #selector(self.receiveAction(sender:)), for: .touchUpInside)
        view.addSubview(receiveButton)
        
        //BGM選択
        BGMmanager.select = 1
        BGMmanager.PlaySound()

        isSrv = false
        connectState = 0
		Advance.AmI1P = false //初期値は2Pで
        
        //非同期処理
        let queue = OperationQueue()
        queue.addOperation() {
            Transmission().connect()    //ソケット準備
            
            isObserving = true
            isReceiving = true
            wantToClose = false
            //通信し続けているか確認(ページ変わってもずっと)
            queue.addOperation() {
                while(isReceiving && isConnecting) {
                    isReceiving = false
                    sleep(7)
                    print("監視")
                }
                if(isConnecting) {
                    wantToClose = true
                }
                isObserving = false
                print("監視終了")
            }
            
            //ユーザー情報登録
            let infoTmp = "custom" + (self.userDefaults.object(forKey: "userName") as? String ?? "未設定") + "($$)1500($$)"
            Transmission().sendMessage(message: infoTmp)
            
            while(connectState == 0 && isConnecting) {
                let readData = Transmission().listenAndRead(rootView: self).components(separatedBy: "($$)")
                
                self.matchList = []  //相手の名前リスト
                self.rateList = []  //相手のレートリスト
                self.keyList = []  //相手のkey
                for i in stride(from:0, to:readData.count - 2, by:3) {
                    self.matchList.append(readData[i])
                    self.rateList.append(readData[i + 1])
                    self.keyList.append(readData[i + 2])
                }
                print(self.matchList)
                OperationQueue.main.addOperation() {
                    self.matchTableView.reloadData()
                }
            }
            
            if(connectState == 1) {				
                OperationQueue.main.addOperation() {
                    self.isPageSift = true
					PageForMatching.syncOpInfo(Name:opponentName , Rate:opponentRate)//敵の情報を次のページに保存
                    self.present(PageForMatching(), animated: true, completion: nil) //ページ遷移
                }
			}
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(isPageSift) {   //matchingページに遷移するとき
            isPageSift = false
        } else {
            if(isConnecting) {
                wantToClose = true
            }
        }
    }
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {
        BGMmanager.StopSound()
        self.dismiss(animated: true, completion: nil)
    }
    
    //1Pになるボタンの挙動
    @objc func receiveAction(sender: UIButton) {
        isSrv = true
        Transmission().sendMessage(message: "srv")
		Advance.AmI1P = true
        receiveButton.isEnabled = false
    }
    
    /*
     Cellが選択された際に呼び出されるデリゲートメソッド.
     */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let queue = OperationQueue()
        queue.addOperation() {
            Transmission().sendMessage(message: self.keyList[indexPath.row])
        }
        opponentName = matchList[indexPath.row]
        opponentRate = rateList[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //再利用するCellを取得する.
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
        
        // Cellに値を設定する.
        cell.textLabel!.text = "\(matchList[indexPath.row]) rate:\(rateList[indexPath.row])"
        
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//非同期処理
class Transmission: Operation {
    //データ受信用メソッド
    func listenAndRead(rootView: UIViewController) -> String {
        var str = "xxx"
        if(listenning) {  //重複してread待ちしないように
            print("listenning中")
            return ""
        }
        listenning = true
        //while(str.pregMatche(pattern: "^x*x$")) {
        while(!str.contains("($)") && !str.contains("($$$)")) { //($)はサーバーからのメッセージ、($$$)は対戦相手からのメッセージ
            if(wantToClose) {   //こちらから切断
                print("stream閉じた")
                isConnecting = false
                inputStream.close()
                outputStream.close()
                listenning = false
                return "end"
            }
            let bufferSize = 1024
            var readBuffer = Array<UInt8>(repeating: 0, count: bufferSize)
            let length = inputStream.read(&readBuffer, maxLength: bufferSize)
            let nsdata = NSData(bytes: readBuffer as [UInt8], length: length)
            str = String(data: nsdata as Data, encoding: String.Encoding.utf8)!
            print(str)
            isReceiving = true  //サーバーから受信が続いていることを保存
        }
        listenning = false
        //if(str.contains("[") && !str.pregReplace(pattern: "[{.}]", with: "").contains("end")) {
        if(str.contains("($)end($)")) {    //切断された
            print("対戦相手が切断")
            if(isConnecting) {
                isConnecting = false
                inputStream.close()
                outputStream.close()
            }
            OperationQueue.main.addOperation() {
                BGMmanager.StopSound()
                if(connectState == 0) {
                    rootView.dismiss(animated: true, completion: nil)
				} else if(connectState == 1) {
					rootView.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
				} else {
					rootView.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            }
        } else if(str.contains("($$$)")) {
            return str.pregReplace(pattern: "($$$)", with: "")
        } else if(str.contains("($)1P($)")) {
            is1P = true
            if(isSrv) {
				let tmp = str.components(separatedBy: "($)")[2]
				opponentName = tmp.components(separatedBy: "($$)")[0]
				opponentRate = tmp.components(separatedBy: "($$)")[1]
            }
            connectState = 1
        } else if(str.contains("($)2P($)")) {
            is1P = false
            if(isSrv) {
                let tmp = str.components(separatedBy: "($)")[2]
                opponentName = tmp.components(separatedBy: "($$)")[0]
                opponentRate = tmp.components(separatedBy: "($$)")[1]
            }
            connectState = 1
        } else if(str.contains("($)false($)")) {    //他のプレイヤーに先に相手を取られた/////////window出すとか
            
        }
        return str.components(separatedBy: "($)")[1]
    }
    
    //データ送信用メソッド
    func sendMessage(message: String) {
        if(isConnecting) {
            guard let buffer = message.data(using: .utf8) else { return }
            _ = buffer.withUnsafeBytes {
                outputStream.write($0, maxLength: buffer.count)
            }
        }
    }
    
    //サーバーとの接続を確立するメソッド
    func connect(){
        var readStream : Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocketToHost(nil, NSString(string: "133.167.88.52"), 1048, &readStream, &writeStream)
        
        inputStream = readStream!.takeRetainedValue()
        outputStream = writeStream!.takeRetainedValue()
        
        inputStream.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
        outputStream.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
        
        inputStream.open()
        outputStream.open()
        
        isConnecting = true
    }
}
