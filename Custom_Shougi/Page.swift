//
//  Page.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/29.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//プレイページ継承用

class Page: UIViewController {
    let configs = UserDefaults.standard //保存データ
    let returnButton = UIButton()   //戻るボタン
    let undoButton = UIButton()  //待ったボタン
    let newButton = UIButton()  //新規対局ボタン
    let firstMove = UIImage(named: "firstMove")
    let firstMoveView1 = UIImageView()
    let firstMoveView2 = UIImageView()
    let passiveMove = UIImage(named: "passiveMove")
    let passiveMoveView1 = UIImageView()
    let passiveMoveView2 = UIImageView()
    var board: PlayBoardView!   //盤
    var backboard: BoardImageView!  //背景と盤の画像
    var pieceView: PieceView!   //駒
    var placeView: MovePlaceView!   //移動可能場所
    var movedLastPieceView: LastMovedPiece! //最後に移動した場所
    
    func makePieceView() {
    }
}
