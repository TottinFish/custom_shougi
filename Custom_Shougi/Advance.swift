//
//  Advance.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/18.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//viewとstateの仲介みたいな役割

///winするのはpagefor2playとかがいいと思う
//成るかどうかのviewを表示中は他のviewをさわれないようにする必要がある & 選択後1度目反応ない?

class Advance: UIView {
    static var root: Page!  //表示しているページのviewcontroller
    static var promoteView: UIView!
    static var promoteViewButton: UIButton!
    static var notPromoteViewButton: UIButton!
    static var isTouched: Bool = false  //移動可能場所を表示中か(初期化必要)////////
    static var touchPieceI: Int = 0 //前にタッチした駒の位置
    static var touchPieceJ: Int = 0
    static var isDebug: Bool = false //デバッグモードかどうか(ターン制かどうか)
    static var isCheckmatedMe: Bool = false    //自分がチェックメイトされてる状態かどうか(初期化必要)////////
    static var isCheckmatedOther: Bool = false    //相手がチェックメイトされてる状態かどうか(初期化必要)////////
    static var isWinnerMe: Bool = false  //自分が勝利の出力に使う
    static var isWinnerOther: Bool = false //相手が勝利の出力に使う
    static var willPromote: Bool = false    //成るかどうかを選択するviewを表示するかどうか
    static var place: [[Bool]] = [[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false]]  //移動可能場所(移動時に再計算しなくていいようにstatic)
    static var Intplace: [[Int]] = [[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0]]  //移動可能場所(移動時に再計算しなくていいようにstatic)
    static var isCheckmate1: Bool = false
    static var isCheckmate2: Bool = false//両王手用
    static var checkmateTaple1: (Int,Int) = (-1,-1)//checkmateを掛けてる駒の座標
    static var checkmateTaple2: (Int,Int) = (-1,-1)//両王手用
    static var LastMoveTaple: (Int,Int) = (-1,-1)//最後に動いた駒の座標を保存
    static var tradeLocate: (Int,Int) = (-1,-1)//動かす駒の位置記録用
    static var isOnline: Bool = false //初期値はオフライン
    static var is1Pfirst: Bool = true //初期値は1P先手
    static var AmI1P : Bool = false //1Pか2Pか判定する(オンラインでのみ使用)
    
    //タッチした場所が移動可能場所かどうかを返す
    static func isAbleToMove(i: Int, j: Int) -> Bool {
        return isTouched && place[i][j]
    }
    
    
    //新たに駒を置ける場所を調べる(歩について調べる必要あり)/////////////
    static func isAbleToPut(kind: Int, isMe: Bool) {
        place = [[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false]]
        var kingX: Int = -1
        var kingY: Int = -1
        
        State.tmpPiece = State.piece(own: isMe, kind: kind, state: true)//置く駒の情報保存
        
        //自分のターンのみ
        if(!isDebug && (State.isMyTurn && !isMe || !State.isMyTurn && isMe)) {
            return
        }
        
        if(isOnline){//オンライン上で相手の駒を動かせないようにする
            if(AmI1P){
                if(is1Pfirst != State.isMyTurn){//1Pが自分のターンじゃないならreturn
                    return
                }
            }else{
                if(is1Pfirst == State.isMyTurn){//2Pが相手のターンならreturn
                    return
                }
            }
        }
        
        //駒のない場所は一旦置けるものとする
        for i in 0..<9 {
            for j in 0..<9 {
                if(State.boardState[i][j] == nil) {
                    place[i][j] = true
                }
                if(State.boardState[i][j] != nil && State.boardState[i][j]?.kind == 7 && State.boardState[i][j]?.own == isMe){
                    if(kingX == -1 && kingY == -1){
                        kingX = i
                        kingY = j
                    }
                }
            }
        }
        
        if(kind == 0 && isMe) {//自分側の2歩ができないようにする
            for i in 0..<9 {
                for j in 0..<9 {
                    if(State.boardState[i][j]?.kind == 0  && State.boardState[i][j]!.own && State.boardState[i][j]?.state == true) {
                        //自分側の歩を見つけたらその縦列をfalseにするお
                        for t in 0..<9{
                            place[i][t] = false
                        }
                    }
                }
            }
        }
        if(kind == 0 && !isMe) {//相手側の2歩ができないようにする
            for i in 0..<9 {
                for j in 0..<9 {
                    if(State.boardState[i][j]?.kind == 0  && !State.boardState[i][j]!.own && State.boardState[i][j]?.state == true) {
                        //自分側の歩を見つけたらその縦列をfalseにする
                        for t in 0..<9{
                            place[i][t] = false
                        }
                    }
                }
            }
        }//関数作るか迷ったけど、歩ぐらいしか2歩とかないから作ってない
        
        //王手時には自分の王を守るようにしか置けないようにする
        
        for i in 0..<9 {
            for j in 0..<9 {
                if(place[i][j] == true) {
                    Intplace = [[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0]]
                    State.tmpboardState = State.boardState//一時保存
                    State.boardState[i][j] = State.tmpPiece
                    // TODO: huhu
                    for x in 0..<9{
                        for y in 0..<9{
                            if(State.boardState[x][y] != nil && State.boardState[x][y]?.own != isMe){
                                //敵の攻撃が王に届きうるのか検証
                                rangeOfEmpire(i: x, j: y, isEmpire: true)
                            }
                        }
                    }
                    if(Intplace[kingX][kingY] == 1){
                        place[i][j] = false
                    }
                    State.boardState = State.tmpboardState
                }
            }
        }
        
        
        if(isMe) {
            touchPieceI = -1    //moveの時のために代入
            touchPieceJ = kind
        } else {
            touchPieceI = -2    //moveの時のために代入
            touchPieceJ = kind
        }
    }
    
    
    //駒を移動
    static func move(i: Int, j: Int) {
        //前の盤面を保存
        var his0 = 0/*ターン*/, his6 = -1/*とったか*/, his7 = 0/*とった駒の裏表*/
        // TODO: 2P側が駒を置こうとするとエラーが出る
        if(touchPieceI == -1 || touchPieceI != -2 && State.boardState[touchPieceI][touchPieceJ] != nil && State.boardState[touchPieceI][touchPieceJ]!.own) {
            his0 = 1
        }
        if(State.boardState[i][j] != nil) {
            his6 = State.boardState[i][j]!.kind
            if(State.boardState[i][j]!.state) {
                his7 = 1
            } else {
                his7 = 0
            }
        }
        var isSelectingPromote = false
        State.history.append([his0, i, j, touchPieceI, touchPieceJ, 0, his6, his7, 0])
        /*State.historyBoard.append(State.boardState)
        State.historyHaving1.append(State.havingPieceNum1)
        State.historyHaving2.append(State.havingPieceNum2)*/
        
        if(State.boardState[i][j] != nil) { //既に駒がある(相手の駒を取る時)の動作
            if(State.boardState[i][j]!.own) {
                State.havingPieceNum2[State.boardState[i][j]!.kind] += 1
            } else {
                State.havingPieceNum1[State.boardState[i][j]!.kind] += 1
            }
        }
        if(touchPieceI == -1) {  //自分の持ち駒
            State.boardState[i][j] = State.piece(own: true, kind: touchPieceJ, state: true)
            State.havingPieceNum1[touchPieceJ] -= 1
        } else if(touchPieceI == -2) {  //相手の持ち駒
            State.boardState[i][j] = State.piece(own: false, kind: touchPieceJ, state: true)
            State.havingPieceNum2[touchPieceJ] -= 1
        } else {
            State.boardState[i][j] = State.boardState[touchPieceI][touchPieceJ]
            State.boardState[touchPieceI][touchPieceJ] = nil
            //成るか選択するviewを表示するかを判定&保存
            if(State.boardState[i][j]!.own && j <= 2 || !State.boardState[i][j]!.own && j >= 6) {
                if(State.boardState[i][j]?.kind != 4 && State.boardState[i][j]?.kind != 7 && State.boardState[i][j]!.state){//金将、王将は成らない 成ってる駒は成らない
                    willPromote = true
                }
                touchPieceI = i
                touchPieceJ = j
                isSelectingPromote = true
            }
        }
        
        //後で復元できるようにplace等を保存しておく
        let placeTmp = place
        let tmpI = touchPieceI
        let tmpJ = touchPieceJ
        //王手状態になったかどうかを判定
        placeCandidate(i: i, j: j, touch:false, roop: false)
        //place等復元
        place = placeTmp
        touchPieceI = tmpI
        touchPieceJ = tmpJ
        
        if(isCheckmate1 && isCheckmate2) {
            State.history[State.history.count - 1][8] = 3
        } else if(isCheckmate1) {
            State.history[State.history.count - 1][8] = 1
        } else if(isCheckmate2) {
            State.history[State.history.count - 1][8] = 2
        }
        
        if(!isSelectingPromote) {
            if(isConnecting) {  //データ送信
                OperationQueue().addOperation() {
                    let tmpHis = State.history[State.history.count - 1]
                    let str = ",\(tmpHis[0]),\(tmpHis[1]),\(tmpHis[2]),\(tmpHis[3]),\(tmpHis[4]),\(tmpHis[5]),\(tmpHis[6]),\(tmpHis[7]),\(tmpHis[8]),"
                    Transmission().sendMessage(message: str)
                }
            } else {
                FileController.saveState()  //history保存
            }
        }
        
        LastMoveTaple = (i,j)
        
        State.isMyTurn = !State.isMyTurn
    }
    
    //受信したデータを元に進行(受信したデータは相手視点)
    //不正データに対する処理も必要!!!!
    static func receiveMove(receiveData: String) {
        let data = receiveData.components(separatedBy: ",")
        var tmp: [Int] = []
        for x in 1..<10 {
            tmp.append(Int(data[x]) ?? 0)
        }
        //0ターン(0が相手の動き,1が自分の動き)
        //1,2移動先の場所
        //3,4移動前の場所(持ち駒なら3に-1か-2,4に駒の種類)
        //5成った瞬間かどうか(成ったなら1,そうでないなら0)
        //6は相手の駒をとったかどうか(取ったならその種類,取ってないなら-1)
        //7は取ったやつの表裏(表が1,裏が0)
        //8は王手かどうか(1が自分のみ,2が相手のみ,3が両方,0が普通)
        if(tmp[3] == -1) {  //持ち駒を移動した
            State.havingPieceNum2[tmp[4]] -= 1
            State.boardState[8 - tmp[1]][8 - tmp[2]] = State.piece(own: false, kind: tmp[4], state: true)
        } else if(tmp[3] == -2) {  //持ち駒を移動した
            State.havingPieceNum1[tmp[4]] -= 1
            State.boardState[8 - tmp[1]][8 - tmp[2]] = State.piece(own: true, kind: tmp[4], state: true)
        } else {    //普通に移動
            State.boardState[8 - tmp[1]][8 - tmp[2]] = State.boardState[8 - tmp[3]][8 - tmp[4]]
            if(tmp[5] == 1) {   //成った瞬間
                State.boardState[8 - tmp[1]][8 - tmp[2]]!.state = false
            }
            State.boardState[8 - tmp[3]][8 - tmp[4]] = nil
            if(tmp[6] != -1) {  //とったか
                if(tmp[0] == 1) {  //持ち駒を増やす
                    State.havingPieceNum2[tmp[6]] += 1
                } else {
                    State.havingPieceNum1[tmp[6]] += 1
                }
            }
        }
        State.isMyTurn = !State.isMyTurn
        /*if(tmp[8] == 0) {   //チェックメイトかどうか
            isCheckmate1 = false
            isCheckmate2 = false
        } else if(tmp[8] == 1) {
            isCheckmate1 = true
            isCheckmate2 = false
        } else if(tmp[8] == 2) {
            isCheckmate1 = false
            isCheckmate2 = true
        } else {
            isCheckmate1 = true
            isCheckmate2 = true
        }*/
        print("処理")
    }
    
    //一つ前へ
    static func undo() {
        //historyは  0ターン(0が相手の動き,1が自分の動き)
        //          1,2移動先の場所
        //          3,4移動前の場所(持ち駒なら3に-1か-2,4に駒の種類)
        //          5成った瞬間かどうか(成ったなら1,そうでないなら0)
        //          6は相手の駒をとったかどうか(取ったならその種類,取ってないなら-1)
        //          7は取ったやつの表裏(表が1,裏が0)
        //          8は王手かどうか(1が自分のみ,2が相手のみ,3が両方,0が普通)
        if(State.history.count != 0) {
            let tmp = State.history[State.history.count - 1]
            if(tmp[3] == -1) {  //持ち駒を移動した
                State.havingPieceNum1[tmp[4]] += 1
            } else if(tmp[3] == -2) {  //持ち駒を移動した
                State.havingPieceNum2[tmp[4]] += 1
            } else {    //普通に移動
                State.boardState[tmp[3]][tmp[4]] = State.boardState[tmp[1]][tmp[2]]
                if(tmp[5] == 1) {   //成った瞬間
                    State.boardState[tmp[3]][tmp[4]]!.state = true
                }
            }
            if(tmp[6] != -1) {  //とったか
                if(tmp[0] == 1) {  //持ち駒を戻す
                    State.havingPieceNum1[tmp[6]] -= 1
                } else {
                    State.havingPieceNum2[tmp[6]] -= 1
                }
                State.boardState[tmp[1]][tmp[2]] = State.piece(own: tmp[0] == 0, kind: tmp[6], state: tmp[7] == 1) //取られた駒を戻す
            } else {
                State.boardState[tmp[1]][tmp[2]] = nil
            }
            if(tmp[8] == 0) {   //チェックメイトかどうか
                isCheckmate1 = false
                isCheckmate2 = false
            } else if(tmp[8] == 1) {
                isCheckmate1 = true
                isCheckmate2 = false
            } else if(tmp[8] == 2) {
                isCheckmate1 = false
                isCheckmate2 = true
            } else {
                isCheckmate1 = true
                isCheckmate2 = true
            }
            State.history.removeLast()  //削除
            FileController.saveState()
        }
    }
    
    
    //成るかどうか選択するwindowの作成
    static func makePromoteView() {
        if(willPromote) {
            promoteView = UIView()
            promoteView.backgroundColor = UIColor.white
            promoteView.frame = CGRect(x:0, y:0, width: 200, height: 250)
            promoteView.layer.position = CGPoint(x: root.view.frame.width/2, y: root.view.frame.height/2)
            promoteView.alpha = 0.8
            promoteView.layer.cornerRadius = 20
            
            promoteViewButton = UIButton()
            promoteViewButton.frame = CGRect(x:0, y:0, width: 50, height: 60)
            promoteViewButton.backgroundColor = UIColor.orange
            promoteViewButton.setTitle("成る", for: .normal)
            promoteViewButton.setTitleColor(UIColor.white, for: .normal)
            promoteViewButton.layer.masksToBounds = true
            promoteViewButton.layer.cornerRadius = 20.0
            promoteViewButton.layer.position = CGPoint(x: promoteView.frame.width/2 - 50, y: promoteView.frame.height-50)
            promoteViewButton.addTarget(self, action: #selector(doPromote(sender:)), for: .touchUpInside)
            promoteView.addSubview(promoteViewButton)
            root.view.addSubview(promoteView)
            
            notPromoteViewButton = UIButton()
            notPromoteViewButton.frame = CGRect(x:0, y:0, width: 70, height: 60)
            notPromoteViewButton.backgroundColor = UIColor.orange
            notPromoteViewButton.setTitle("成らない", for: .normal)
            notPromoteViewButton.setTitleColor(UIColor.white, for: .normal)
            notPromoteViewButton.layer.masksToBounds = true
            notPromoteViewButton.layer.cornerRadius = 20.0
            notPromoteViewButton.layer.position = CGPoint(x: promoteView.frame.width/2 + 50, y: promoteView.frame.height-50)
            notPromoteViewButton.addTarget(self, action: #selector(notPromote(sender:)), for: .touchUpInside)
            promoteView.addSubview(notPromoteViewButton)
            
            root.view.addSubview(promoteView)
        }
    }
    
    
    //成るボタンの挙動
    @objc static func doPromote(sender: UIButton) {
        State.boardState[touchPieceI][touchPieceJ]!.state = false   //状態変更
        willPromote = false
        promoteViewButton.removeFromSuperview()
        promoteView.removeFromSuperview()
        //王手かどうか
        if(!State.boardState[touchPieceI][touchPieceJ]!.own) {
            isCheckmatedMe = false
        } else {
            isCheckmatedOther = false
        }
        placeCandidate(i:touchPieceI, j:touchPieceJ, touch: false, roop: false)
        State.history[State.history.count - 1][5] = 1
        if(isConnecting) {  //データ送信
            OperationQueue().addOperation() {
                let tmpHis = State.history[State.history.count - 1]
                let str = ",\(tmpHis[0]),\(tmpHis[1]),\(tmpHis[2]),\(tmpHis[3]),\(tmpHis[4]),\(tmpHis[5]),\(tmpHis[6]),\(tmpHis[7]),\(tmpHis[8]),"
                Transmission().sendMessage(message: str)
            }
        } else {
            FileController.saveState()  //history保存
        }
        //再描画
        root.pieceView.removeFromSuperview()
        root.makePieceView()
    }
    
    //成らないボタンの挙動
    @objc static func notPromote(sender: UIButton) {
        willPromote = false
        
        if(isConnecting) {  //データ送信
            OperationQueue().addOperation() {
                let tmpHis = State.history[State.history.count - 1]
                let str = ",\(tmpHis[0]),\(tmpHis[1]),\(tmpHis[2]),\(tmpHis[3]),\(tmpHis[4]),\(tmpHis[5]),\(tmpHis[6]),\(tmpHis[7]),\(tmpHis[8]),"
                Transmission().sendMessage(message: str)
            }
        } else {
            FileController.saveState()  //history保存
        }
        promoteViewButton.removeFromSuperview()
        promoteView.removeFromSuperview()
    }
    
    
    //盤上の駒をタッチした時に移動可能場所を探す(placeに保存)(王手状態になったかどうか調べるのにも使う)
    static func placeCandidate(i: Int, j: Int, touch: Bool, roop: Bool) {
        //動ける範囲のplaceをtrueにする
        touchPieceI = i //タッチした駒の位置を保存
        touchPieceJ = j
        place = [[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false]]
        
        let isMine = State.boardState[i][j]?.own ?? true
        var isMineInt: Int = 1
        if(!isMine) {
            isMineInt = -1;    //示す移動可能場所の相対値を反転させるため
        }
        
        if(tradeLocate.0 == -1 && tradeLocate.1 == -1){
            tradeLocate = (i,j)//対象駒の位置を覚えておく
        }
        State.tmpPiece = State.boardState[i][j]!
        
        //自分のターンのみ
        if(!isDebug && touch && (State.isMyTurn && !isMine || !State.isMyTurn && isMine)) {
            return
        }
        
        if(isOnline){//オンライン上で相手の駒を動かせないようにする
            if(AmI1P){
                if(is1Pfirst != State.isMyTurn){//1Pが自分のターンじゃないならreturn
                    return
                }
            }else{
                if(is1Pfirst == State.isMyTurn){//2Pが相手のターンならreturn
                    return
                }
            }
        }
        
        switch State.boardState[i][j]?.kind{
        case 0? :   //歩兵
            if(State.boardState[i][j]!.state) {
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
            } else {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            }
        case 1? :   //香車
            if(State.boardState[i][j]!.state) {
                for k in 1..<9 {
                    Advance.safeSubstitution(place: &place, i: i, j: j - k * isMineInt, isMine: isMine)
                    if(!Advance.isTherePieceOrNoBoard(i: i, j: j - k * isMineInt)) {  //もし駒があったらそこまで
                        break
                    }
                }
            } else {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            }
        case 2? :   //桂馬
            if(State.boardState[i][j]!.state) {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 2 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 2 * isMineInt, isMine: isMine)
            } else {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            }
        case 3? :   //銀将
            if(State.boardState[i][j]!.state) {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j + 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j + 1 * isMineInt, isMine: isMine)
            } else {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            }
        case 4? :   //金将
            Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
        case 5? :   //角行
            if(!State.boardState[i][j]!.state) {
                Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i - k, j: j - k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i - k, j: j - k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i + k, j: j - k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i + k, j: j - k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i - k, j: j + k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i - k, j: j + k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i + k, j: j + k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i + k, j: j + k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
        case 6? :   //飛車
            if(!State.boardState[i][j]!.state) {
                Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i - 1, j: j + 1 * isMineInt, isMine: isMine)
                Advance.safeSubstitution(place: &place, i: i + 1, j: j + 1 * isMineInt, isMine: isMine)
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i, j: j - k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i, j: j - k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i, j: j + k * isMineInt, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i, j: j + k * isMineInt)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i - k, j: j, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i - k, j: j)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.safeSubstitution(place: &place, i: i + k, j: j, isMine: isMine)
                if(!Advance.isTherePieceOrNoBoard(i: i + k, j: j)) {  //もし駒があったらそこまで
                    break
                }
            }
        case 7? :   //王将
            Advance.safeSubstitution(place: &place, i: i - 1, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i + 1, j: j - 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i - 1, j: j, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i + 1, j: j, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i - 1, j: j + 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i, j: j + 1 * isMineInt, isMine: isMine)
            Advance.safeSubstitution(place: &place, i: i + 1, j: j + 1 * isMineInt, isMine: isMine)
        default : break
        }
        
        //王手の判定
        judgeCheckmate(i: i, j: j, touch: touch, roop: roop)
        //対象駒の位置初期化
        tradeLocate = (-1,-1)
    }
    
    //指定した場所が配列内ならtrueに
    static func safeSubstitution(place: inout[[Bool]], i: Int, j: Int, isMine: Bool) {
        Intplace = [[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0]]
        var kingX: Int = -1
        var kingY: Int = -1
        
        if(i >= 0 && i <= 8 && j >= 0 && j <= 8 && State.boardState[i][j]?.own != isMine) {
            place[i][j] = true
            State.tmpboardState = State.boardState//盤面の状況を一時的に保存しておく
            //駒を動かした時に王が殺されるか判定するために一時的に動かす
            State.boardState[tradeLocate.0][tradeLocate.1] = nil
            State.boardState[i][j] = State.tmpPiece
            for x in 0..<9{
                for y in 0..<9{
                    if(State.boardState[x][y] != nil && State.boardState[x][y]?.own != isMine){
                        //敵の攻撃が王に届きうるのか検証
                        rangeOfEmpire(i: x, j: y, isEmpire: true)
                    }
                    if(State.boardState[x][y] != nil && State.boardState[x][y]?.own == isMine && State.boardState[x][y]?.kind == 7){
                        //王さまの位置を記録
                            kingX = x
                            kingY = y
                    }
                }
            }
            if(kingX != -1 && kingY != -1 && Intplace[kingX][kingY] == 1){
                place[i][j] = false
            }
            State.boardState = State.tmpboardState//盤面をもとに戻す
        }
    }
    
    //駒があったらそこまでしかtrueにしないように(まだ先を見たほうがいいときはtrue)
    static func isTherePieceOrNoBoard(i: Int, j: Int) -> Bool{
        return (i >= 0 && i <= 8 && j >= 0 && j <= 8 && State.boardState[i][j] == nil)
    }
    
    
    //王手を判定する関数
    static func judgeCheckmate(i: Int, j: Int, touch: Bool, roop: Bool) {
        //後で復元できるようにplace等を保存しておく
        let placeTmp = place    
        let tmpI = touchPieceI
        let tmpJ = touchPieceJ
        
        if(touch) { //王手されてないのに移動後に王手される状態(移動可能にしないよう)
            
        } else if(!roop) {   //移動後に王手しているか(最初はこっち)
            let tmpMe = isCheckmatedMe
            let tmpOther = isCheckmatedOther
            isCheckmatedMe = false
            isCheckmatedOther = false
            afterMoveCheckmate(i: i, j: j)
            if(isCheckmatedMe && tmpMe) {
                isWinnerOther = true
                print("2Pの勝ち")
            } else if(isCheckmatedOther && tmpOther) {
                isWinnerMe = true
                print("1Pの勝ち")
            } else if(isCheckmatedMe) {
                print("2Pの王手")
            } else if(isCheckmatedOther) {
                print("1Pが王手")
            }
        } else {    //移動後に王手しているか(再帰中)
            //afterMoveCheckmateで呼ばれた時に入る
            for x in 0..<9 {
                for y in 0..<9 {
                    if(place[x][y] && State.boardState[x][y] != nil && State.boardState[x][y]!.kind == 7) {
                        //敵が動ける位置に自分のキングがいたら王手がかかる(または逆)
                        if(State.boardState[i][j]!.own) {
                            isCheckmatedOther = true
                            
                        } else {
                            isCheckmatedMe = true
                        }
                        
                    }
                }
            }
        }
        
        //place等復元
        place = placeTmp
        touchPieceI = tmpI
        touchPieceJ = tmpJ
    }
    
    static func afterMoveCheckmate(i: Int, j: Int) {
        isCheckmatedMe = false
        isCheckmatedOther = false
        for x in 0..<9 {    //x,yは相手の駒の位置と想定
            for y in 0..<9 {
                if(State.boardState[x][y] != nil && !State.boardState[x][y]!.own) {
                    placeCandidate(i: x, j: y, touch: false, roop: true)
                    if(isCheckmatedMe) {
                        
                    }
                } else if(State.boardState[x][y] != nil && State.boardState[x][y]!.own) {
                    placeCandidate(i: x, j: y, touch: false, roop: true)
                    if(isCheckmatedOther) {
                        
                    }
                }
            }
        }
    }
    
    static func ResetAdvance() {//新規対局の時に初期化する
        isCheckmatedMe = false
        isCheckmatedOther = false
        isWinnerMe = false  //自分が勝利のPrintに使う
        isWinnerOther = false //相手が勝利のPrint
    }
    
    static func judgeEnd(isMe: Bool){//詰み状態かを判断する関数
        Intplace = [[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0, 0]]
        //isMeがtrueのとき自分の王が詰んでいるか判断する
        /*
         TODO Int型の盤面IntPlaceを作成
         Boolでやりたいけど持ち駒での王手回避を考えるとIntになった
         0:王手されている王の逃げ場所
         1:相手(王手を掛けている側)が動けるところ。
         2:王手を防げる持ち駒がおける場所、自陣の駒で守れる範囲
         先に2を配置して、それを直線移動系駒の停止点にする
         */
        var kingx: Int = 0
        var kingy: Int = 0
        var safe = false
        isCheckmate1 = false
        isCheckmate2 = false    //両王手用
        checkmateTaple1 = (-1,-1)
        checkmateTaple2 = (-1,-1)
        if(isMe){
            for i in 0..<8{//置ける持ち駒があるか調べる
                if(State.havingPieceNum1[i] != 0){
                    isAbleToPut(kind: i, isMe: true)
                }
            }
        }else{
            for i in 0..<8{//置ける持ち駒があるか調べる
                if(State.havingPieceNum2[i] != 0){
                    isAbleToPut(kind: i, isMe: false)
                }
            }
        }
        
        for a in 0..<9{
            for b in 0..<9{
                if(place[a][b]) {//盤面のどこに駒が置けるかplaceのtrueになっているからそれをIntplaceに同期する
                    Intplace[a][b] = 2
                }
                if(State.boardState[a][b] != nil && State.boardState[a][b]!.own == isMe && State.boardState[a][b]!.kind != 7) {//自陣の駒が支配して(守って)るところ
                    rangeOfEmpire(i: a, j: b, isEmpire: false)
                }else if(State.boardState[a][b] != nil && State.boardState[a][b]!.own == isMe && State.boardState[a][b]!.kind == 7){
                    //王の位置を覚えておく
                    kingx = a
                    kingy = b
                }
            }
        }
        
        for x in 0..<9{
            for y in 0..<9{
                if(State.boardState[x][y] != nil && State.boardState[x][y]!.own != isMe) {//相手の駒がどこまで支配しているか調べる
                    rangeOfEmpire(i: x, j: y, isEmpire: true)
                }
            }
        }
        
        for x in 0..<9{
            for y in 0..<9{
                if(State.boardState[x][y] != nil && State.boardState[x][y]!.own == isMe && State.boardState[x][y]!.kind != 7) {//自陣の駒で王を守れる範囲を調べる
                    rangeOfEmpire(i: x, j: y, isEmpire: false)
                }
            }
        }
        
        //王の左上のマスから探索を始める
        for xtimes in 0..<3{//王の位置が1、周り全ても1なら詰み
            for ytimes in 0..<3{
                let tmpx = kingx - 1 + xtimes
                let tmpy = kingy - 1 + ytimes
                if(isKingEnd(i: tmpx, j: tmpy)){
                    print("tmpx: \(tmpx)  tmpy: \(tmpy) ")
                    print(Intplace[tmpx][tmpy])
                    if(State.boardState[tmpx][tmpy] == nil && (Intplace[tmpx][tmpy] == 0 || Intplace[tmpx][tmpy] == 2)){
                        print("safe changed to true 1")
                        safe = true
                    }
                    if(State.boardState[tmpx][tmpy] != nil && State.boardState[tmpx][tmpy]!.own != isMe && (Intplace[tmpx][tmpy] == 0 ||
                        Intplace[tmpx][tmpy] == 2)){
                        print("safe changed to true 2")
                        safe = true
                    }
                }
            }
        }
        
        if(checkmateTaple1.0 > 0 && checkmateTaple1.1 > 0){//王手を掛けてる駒が取れるならばセーフ
            if(Intplace[checkmateTaple1.0][checkmateTaple1.1] == 2){
                print("safe changed to true 3")
                safe = true
            }
        }
        
        if(checkmateTaple2.0 > 0 && checkmateTaple2.1 > 0){//王手を掛けてる駒が取れるならばセーフ
            if(Intplace[checkmateTaple2.0][checkmateTaple2.1] == 2){
                print("safe changed to true 4")
                print("Intplace taple2 x")
                print(checkmateTaple2.0)
                print("Intplace taple2 y")
                print(checkmateTaple2.1)
                safe = true
            }
        }
        
        if(!safe){
            if(isMe){
                isWinnerOther = true
            }else{
                isWinnerMe = true
            }
        }
        
    }
    
    static func rangeOfEmpire(i: Int, j: Int, isEmpire: Bool){//駒のいける範囲全てのplaceをtrueにする
        let isMine = State.boardState[i][j]?.own ?? true
        var isMineInt: Int = 1
        if(!isMine) {
            isMineInt = -1;    //示す移動可能場所の相対値を反転させるため
        }
        
        //自分のターンのみ
        /*if(!isDebug && (State.isMyTurn && !isMine || !State.isMyTurn && isMine)) {
            return
        }*/
        
        switch State.boardState[i][j]?.kind{
        case 0? :   //歩兵
            if(State.boardState[i][j]!.state) {
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            } else {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
        case 1? :   //香車
            if(State.boardState[i][j]!.state) {
                for k in 1..<9 {
                    Advance.empireThere(Intplace: &Intplace, i: i, j: j - k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                    if(!Advance.isThereStop(i: i, j: j - k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                        break
                    }
                }
            } else {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
        case 2? :   //桂馬
            if(State.boardState[i][j]!.state) {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 2 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 2 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            } else {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
        case 3? :   //銀将
            if(State.boardState[i][j]!.state) {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            } else {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
        case 4? :   //金将
            Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
        case 5? :   //角行
            if(!State.boardState[i][j]!.state) {
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i - k, j: j - k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i - k, j: j - k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i + k, j: j - k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i + k, j: j - k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i - k, j: j + k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i - k, j: j + k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i + k, j: j + k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i + k, j: j + k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
        case 6? :   //飛車
            if(!State.boardState[i][j]!.state) {
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
                Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i, j: j - k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i, j: j - k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i, j: j + k * isMineInt, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i, j: j + k * isMineInt, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i - k, j: j, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i - k, j: j, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
            for k in 1..<9 {
                Advance.empireThere(Intplace: &Intplace, i: i + k, j: j, isMine: isMine, isEmpire: isEmpire)
                if(!Advance.isThereStop(i: i + k, j: j, isEmpire: isEmpire)) {  //もし駒があったらそこまで
                    break
                }
            }
        case 7? :   //王将
            Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j - 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i - 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
            Advance.empireThere(Intplace: &Intplace, i: i + 1, j: j + 1 * isMineInt, isMine: isMine, isEmpire: isEmpire)
        default : break
        }
        
        if(isCheckmate1){
            if(checkmateTaple1.0 > 0 && checkmateTaple1.1 > 0){
                checkmateTaple1 = (i,j)
            }
        }
        if(isCheckmate2){
            if(checkmateTaple2.0 > 0 && checkmateTaple2.1 > 0){
                checkmateTaple2 = (i,j)
                print("Register taple2 x")
                print(checkmateTaple2.0)
                print("Register taple2 y")
                print(checkmateTaple2.1)
            }
        }
        
    }
    
    //指定した場所が配列内ならtrueに
    static func empireThere(Intplace: inout[[Int]], i: Int, j: Int, isMine: Bool, isEmpire: Bool) {//isEmpireがtrueならば相手の支配領域調査
        if(i >= 0 && i <= 8 && j >= 0 && j <= 8) {
            if(isEmpire){
                Intplace[i][j] = 1
                if(State.boardState[i][j] != nil && State.boardState[i][j]!.kind == 7){
                    if(!isCheckmate1){
                        isCheckmate1 = true
                    }else{
                        isCheckmate2 = true
                    }
                }
            }else{
                if(State.boardState[i][j] == nil && Intplace[i][j]  == 1){
                }else{
                    Intplace[i][j] = 2
                }
            }
        }
    }
    
    //持ち駒が置ける範囲があったらそこまでしかtrueにしないように(まだ先を見たほうがいいときはtrue)
    static func isThereStop(i: Int, j: Int, isEmpire: Bool) -> Bool{//自分の駒がいけるところまで
        return (i >= 0 && i <= 8 && j >= 0 && j <= 8 && State.boardState[i][j] == nil && (Intplace[i][j] != 2 || !isEmpire))
    }
    
    static func isKingEnd(i: Int, j: Int)->Bool{//王様の周りを見る
        return(i >= 0 && i <= 8 && j >= 0 && j <= 8)
    }
    
}
