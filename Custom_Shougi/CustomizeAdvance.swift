//
//  CustomizeAdvance.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2019/02/12.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

//custom画面のadvance(変数はAdvanceのものを使用)

class CustomizeAdvance: UIView {
    //タッチした場所が移動可能場所かどうかを返す
    static func isAbleToMove(i: Int, j: Int) -> Bool {
        return Advance.isTouched && Advance.place[i][j]
    }
    
    //新たに駒を置ける場所を調べる(歩について調べる必要あり)/////////////
    /*static func isAbleToPut(kind: Int, isMe: Bool) {
        place = [[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false]]
    }*/
    
    
    //駒を移動
    static func move(i: Int, j: Int) {
        if(State.boardState[i][j] != nil) { //既に駒がある(駒を反転)の動作
            let tmp = State.boardState[i][j]
            State.boardState[i][j] = State.boardState[Advance.touchPieceI][Advance.touchPieceJ]
            State.boardState[Advance.touchPieceI][Advance.touchPieceJ] = tmp
            /*if(State.boardState[i][j]!.own) {
                State.havingPieceNum2[State.boardState[i][j]!.kind] += 1
            } else {
                State.havingPieceNum1[State.boardState[i][j]!.kind] += 1
            }*/
        } else {
            State.boardState[i][j] = State.boardState[Advance.touchPieceI][Advance.touchPieceJ]
            State.boardState[Advance.touchPieceI][Advance.touchPieceJ] = nil
        }
        /*if(touchPieceI == -1) {  //自分の持ち駒
            State.boardState[i][j] = State.piece(own: true, kind: touchPieceJ, state: true)
            State.havingPieceNum1[touchPieceJ] -= 1
        } else if(touchPieceI == -2) {  //相手の持ち駒
            State.boardState[i][j] = State.piece(own: false, kind: touchPieceJ, state: true)
            State.havingPieceNum2[touchPieceJ] -= 1
        } else {*/
    }
    
    //盤上の駒をタッチした時に移動可能場所を探す(placeに保存)
    static func placeCandidate(i: Int, j: Int, isMe: Bool) {
        Advance.touchPieceI = i //タッチした駒の位置を保存
        Advance.touchPieceJ = j
        Advance.place = [[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false],[false, false, false, false, false, false, false, false, false]]
        
        if(isMe) {
            for i in 0..<9 {
                for j in 6..<9 {
                    Advance.place[i][j] = true
                }
            }
        } else {
            for i in 0..<9 {
                for j in 0..<3 {
                    Advance.place[i][j] = true
                }
            }
        }
        
        for x in 0..<9{
            for y in 0..<9{
                if(State.boardState[x][y]?.kind == 0){
                    Advance.place[x][y] = false
                }
            }
        }
        
        if(State.boardState[Advance.touchPieceI][Advance.touchPieceJ] != nil && State.boardState[Advance.touchPieceI][Advance.touchPieceJ]?.kind == 0){
            //print("pawn was touched")
            for x in 0..<9{//2歩しちゃダメやから
                for y in 0..<9{
                    Advance.place[x][y] = false
                }
            }
            //歩は1歩前までだけ出れる
            if(isMe){
                if(Advance.touchPieceJ == 6 && State.boardState[Advance.touchPieceI][5] == nil){
                    Advance.place[Advance.touchPieceI][5] = true
                }else if(Advance.touchPieceJ == 5 && State.boardState[Advance.touchPieceI][6] == nil){
                    Advance.place[Advance.touchPieceI][6] = true
                }
            }else{
                if(Advance.touchPieceJ == 2 && State.boardState[Advance.touchPieceI][3] == nil){
                    Advance.place[Advance.touchPieceI][3] = true
                }else if(Advance.touchPieceJ == 3 && State.boardState[Advance.touchPieceI][2] == nil){
                    Advance.place[Advance.touchPieceI][2] = true
                }
            }
        }
        
    }
    
    //指定した場所が配列内ならtrueに
    /*static func safeSubstitution(place: inout[[Bool]], i: Int, j: Int, isMine: Bool) {
        if(i >= 0 && i <= 8 && j >= 0 && j <= 8 && State.boardState[i][j]?.own != isMine) {
            place[i][j] = true
        }
    }*/
}
