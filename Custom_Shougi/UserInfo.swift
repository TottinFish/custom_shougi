//
//  UserInfo.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/05.
//  Copyright © 2019 Totty. All rights reserved.
//

import Foundation

class UserInfo {
  //どの駒の種類を使えるのかのフラグ
    static var normal: Bool = true  //初めはノーマルしか使えない 0
    //デバッグのため今は全部使える
    static var red: Bool = true         //1
    static var orange: Bool = true      //2
    static var yellow: Bool = true      //3
    static var green: Bool = true       //4
    static var blue: Bool = true        //5
    static var indigo: Bool = true      //6
    static var purple: Bool = true      //7
    static var pink: Bool = true        //8
    static var black: Bool = true       //9
    static var white: Bool = true       //10
    static var silver: Bool = true      //11
    static var gold: Bool = true        //12
    static var Army: Bool = true        //13
    static var blueArmy: Bool = true    //14
    static var orangeArmy: Bool = true  //15
    static var grayArmy: Bool = true    //16
    static var goldArmy: Bool = true    //17
    static var hadesArmy: Bool = true   //18
    static var greenArmy: Bool = true   //19
    static var Fire: Bool = true        //20
    static var blueFire: Bool = true    //21
    static var orangeFire: Bool = true  //22
    static var grayFire: Bool = true    //23
    static var goldFire: Bool = true    //24
    static var hadesFire: Bool = true   //25
    static var greenFire: Bool = true   //26
    //選んでいる駒の種類は何か数字で管理対応の数字は上記
    static var setPiece: Int = 0  //1P用
    static var SubsetPiece: Int = 0  //2P用
    
    static let PieceBool: [Bool] = [normal,red,orange,yellow,green,blue,indigo,purple,pink,
                                    black,white,silver,gold,Army,blueArmy,orangeArmy,grayArmy,
                                    goldArmy,hadesArmy,greenArmy,Fire,blueFire,orangeFire,
                                    grayFire,goldFire,hadesFire,greenFire]
    
}
