//
//  ImageData.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/05.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit


class ImageData {
    /************  駒情報始まり   **************/
    static let NormalImage: [UIImage] = [UIImage(named: "hohei")!, UIImage(named: "kyousya")!, UIImage(named: "keima")!, UIImage(named: "ginsyou")!, UIImage(named: "kinsyou")!, UIImage(named: "kaku")!, UIImage(named: "hisya")!, UIImage(named: "ousyou")!]
    
    static let AdvanceNormalImage: [UIImage] = [UIImage(named: "tokin")!, UIImage(named: "narika")!, UIImage(named: "narikei")!, UIImage(named: "narigin")!, UIImage(named: "kinsyou")!, UIImage(named: "ryuuma")!, UIImage(named: "ryuuou")!, UIImage(named: "ousyou")!]
    
    static let RedImage: [UIImage] = [UIImage(named: "redhohei")!, UIImage(named: "redkyousya")!, UIImage(named: "redkeima")!, UIImage(named: "redginsyou")!, UIImage(named: "redkinsyou")!, UIImage(named: "redkaku")!, UIImage(named: "redhisya")!, UIImage(named: "redousyou")!]
    
    static let AdvanceRedImage: [UIImage] = [UIImage(named: "redtokin")!, UIImage(named: "rednarika")!, UIImage(named: "rednarikei")!, UIImage(named: "rednarigin")!, UIImage(named: "redkinsyou")!, UIImage(named: "redryuuma")!, UIImage(named: "redryuuou")!, UIImage(named: "redousyou")!]
    
    static let OrangeImage: [UIImage] = [UIImage(named: "orangehohei")!, UIImage(named: "orangekyousya")!, UIImage(named: "orangekeima")!, UIImage(named: "orangeginsyou")!, UIImage(named: "orangekinsyou")!, UIImage(named: "orangekaku")!, UIImage(named: "orangehisya")!, UIImage(named: "orangeousyou")!]
    
    static let AdvanceOrangeImage: [UIImage] = [UIImage(named: "orangetokin")!, UIImage(named: "orangenarika")!, UIImage(named: "orangenarikei")!, UIImage(named: "orangenarigin")!, UIImage(named: "orangekinsyou")!, UIImage(named: "orangeryuuma")!, UIImage(named: "orangeryuuou")!, UIImage(named: "orangeousyou")!]
    
    static let YellowImage: [UIImage] = [UIImage(named: "yellowhohei")!, UIImage(named: "yellowkyousya")!, UIImage(named: "yellowkeima")!, UIImage(named: "yellowginsyou")!, UIImage(named: "yellowkinsyou")!, UIImage(named: "yellowkaku")!, UIImage(named: "yellowhisya")!, UIImage(named: "yellowousyou")!]
    
    static let AdvanceYellowImage: [UIImage] = [UIImage(named: "yellowtokin")!, UIImage(named: "yellownarika")!, UIImage(named: "yellownarikei")!, UIImage(named: "yellownarigin")!, UIImage(named: "yellowkinsyou")!, UIImage(named: "yellowryuuma")!, UIImage(named: "yellowryuuou")!, UIImage(named: "yellowousyou")!]
    
    static let GreenImage: [UIImage] = [UIImage(named: "greenhohei")!, UIImage(named: "greenkyousya")!, UIImage(named: "greenkeima")!, UIImage(named: "greenginsyou")!, UIImage(named: "greenkinsyou")!, UIImage(named: "greenkaku")!, UIImage(named: "greenhisya")!, UIImage(named: "greenousyou")!]
    
    static let AdvanceGreenImage: [UIImage] = [UIImage(named: "greentokin")!, UIImage(named: "greennarika")!, UIImage(named: "greennarikei")!, UIImage(named: "greennarigin")!, UIImage(named: "greenkinsyou")!, UIImage(named: "greenryuuma")!, UIImage(named: "greenryuuou")!, UIImage(named: "greenousyou")!]
    
    static let BlueImage: [UIImage] = [UIImage(named: "bluehohei")!, UIImage(named: "bluekyousya")!, UIImage(named: "bluekeima")!, UIImage(named: "blueginsyou")!, UIImage(named: "bluekinsyou")!, UIImage(named: "bluekaku")!, UIImage(named: "bluehisya")!, UIImage(named: "blueousyou")!]
    
    static let AdvanceBlueImage: [UIImage] = [UIImage(named: "bluetokin")!, UIImage(named: "bluenarika")!, UIImage(named: "bluenarikei")!, UIImage(named: "bluenarigin")!, UIImage(named: "bluekinsyou")!, UIImage(named: "blueryuuma")!, UIImage(named: "blueryuuou")!, UIImage(named: "blueousyou")!]
    
    static let IndigoImage: [UIImage] = [UIImage(named: "indigohohei")!, UIImage(named: "indigokyousya")!, UIImage(named: "indigokeima")!, UIImage(named: "indigoginsyou")!, UIImage(named: "indigokinsyou")!, UIImage(named: "indigokaku")!, UIImage(named: "indigohisya")!, UIImage(named: "indigoousyou")!]
    
    static let AdvanceIndigoImage: [UIImage] = [UIImage(named: "indigotokin")!, UIImage(named: "indigonarika")!, UIImage(named: "indigonarikei")!, UIImage(named: "indigonarigin")!, UIImage(named: "indigokinsyou")!, UIImage(named: "indigoryuuma")!, UIImage(named: "indigoryuuou")!, UIImage(named: "indigoousyou")!]
    
    static let PurpleImage: [UIImage] = [UIImage(named: "purplehohei")!, UIImage(named: "purplekyousya")!, UIImage(named: "purplekeima")!, UIImage(named: "purpleginsyou")!, UIImage(named: "purplekinsyou")!, UIImage(named: "purplekaku")!, UIImage(named: "purplehisya")!, UIImage(named: "purpleousyou")!]
    
    static let AdvancePurpleImage: [UIImage] = [UIImage(named: "purpletokin")!, UIImage(named: "purplenarika")!, UIImage(named: "purplenarikei")!, UIImage(named: "purplenarigin")!, UIImage(named: "purplekinsyou")!, UIImage(named: "purpleryuuma")!, UIImage(named: "purpleryuuou")!, UIImage(named: "purpleousyou")!]
    
    static let PinkImage: [UIImage] = [UIImage(named: "pinkhohei")!, UIImage(named: "pinkkyousya")!, UIImage(named: "pinkkeima")!, UIImage(named: "pinkginsyou")!, UIImage(named: "pinkkinsyou")!, UIImage(named: "pinkkaku")!, UIImage(named: "pinkhisya")!, UIImage(named: "pinkousyou")!]
    
    static let AdvancePinkImage: [UIImage] = [UIImage(named: "purpletokin")!, UIImage(named: "purplenarika")!, UIImage(named: "purplenarikei")!, UIImage(named: "purplenarigin")!, UIImage(named: "purplekinsyou")!, UIImage(named: "purpleryuuma")!, UIImage(named: "purpleryuuou")!, UIImage(named: "purpleousyou")!]
    
    static let BlackImage: [UIImage] = [UIImage(named: "blackhohei")!, UIImage(named: "blackkyousya")!, UIImage(named: "blackkeima")!, UIImage(named: "blackginsyou")!, UIImage(named: "blackkinsyou")!, UIImage(named: "blackkaku")!, UIImage(named: "blackhisya")!, UIImage(named: "blackousyou")!]
    
    static let AdvanceBlackImage: [UIImage] = [UIImage(named: "blacktokin")!, UIImage(named: "blacknarika")!, UIImage(named: "blacknarikei")!, UIImage(named: "blacknarigin")!, UIImage(named: "blackkinsyou")!, UIImage(named: "blackryuuma")!, UIImage(named: "blackryuuou")!, UIImage(named: "blackousyou")!]
    
    static let WhiteImage: [UIImage] = [UIImage(named: "whitehohei")!, UIImage(named: "whitekyousya")!, UIImage(named: "whitekeima")!, UIImage(named: "whiteginsyou")!, UIImage(named: "whitekinsyou")!, UIImage(named: "whitekaku")!, UIImage(named: "whitehisya")!, UIImage(named: "whiteousyou")!]
    
    static let AdvanceWhiteImage: [UIImage] = [UIImage(named: "whitetokin")!, UIImage(named: "whitenarika")!, UIImage(named: "whitenarikei")!, UIImage(named: "whitenarigin")!, UIImage(named: "whitekinsyou")!, UIImage(named: "whiteryuuma")!, UIImage(named: "whiteryuuou")!, UIImage(named: "whiteousyou")!]
    
    static let SilverImage: [UIImage] = [UIImage(named: "silverhohei")!, UIImage(named: "silverkyousya")!, UIImage(named: "silverkeima")!, UIImage(named: "silverginsyou")!, UIImage(named: "silverkinsyou")!, UIImage(named: "silverkaku")!, UIImage(named: "silverhisya")!, UIImage(named: "silverousyou")!]
    
    static let AdvanceSilverImage: [UIImage] = [UIImage(named: "silvertokin")!, UIImage(named: "silvernarika")!, UIImage(named: "silvernarikei")!, UIImage(named: "silvernarigin")!, UIImage(named: "silverkinsyou")!, UIImage(named: "silverryuuma")!, UIImage(named: "silverryuuou")!, UIImage(named: "silverousyou")!]
    
    static let GoldImage: [UIImage] = [UIImage(named: "goldhohei")!, UIImage(named: "goldkyousya")!, UIImage(named: "goldkeima")!, UIImage(named: "goldginsyou")!, UIImage(named: "goldkinsyou")!, UIImage(named: "goldkaku")!, UIImage(named: "goldhisya")!, UIImage(named: "goldousyou")!]
    
    static let AdvanceGoldImage: [UIImage] = [UIImage(named: "goldtokin")!, UIImage(named: "goldnarika")!, UIImage(named: "goldnarikei")!, UIImage(named: "goldnarigin")!, UIImage(named: "goldkinsyou")!, UIImage(named: "goldryuuma")!, UIImage(named: "goldryuuou")!, UIImage(named: "goldousyou")!]
    
    static let ArmyImage: [UIImage] = [UIImage(named: "armyhohei")!, UIImage(named: "armykyousya")!, UIImage(named: "armykeima")!, UIImage(named: "armyginsyou")!, UIImage(named: "armykinsyou")!, UIImage(named: "armykaku")!, UIImage(named: "armyhisya")!, UIImage(named: "armyousyou")!]
    
    static let AdvanceArmyImage: [UIImage] = [UIImage(named: "armytokin")!, UIImage(named: "armynarika")!, UIImage(named: "armynarikei")!, UIImage(named: "armynarigin")!, UIImage(named: "armykinsyou")!, UIImage(named: "armyryuuma")!, UIImage(named: "armyryuuou")!, UIImage(named: "armyousyou")!]
    
    static let BlueArmyImage: [UIImage] = [UIImage(named: "bluearmyhohei")!, UIImage(named: "bluearmykyousya")!, UIImage(named: "bluearmykeima")!, UIImage(named: "bluearmyginsyou")!, UIImage(named: "bluearmykinsyou")!, UIImage(named: "bluearmykaku")!, UIImage(named: "bluearmyhisya")!, UIImage(named: "bluearmyousyou")!]
    
    static let AdvanceBlueArmyImage: [UIImage] = [UIImage(named: "bluearmytokin")!, UIImage(named: "bluearmynarika")!, UIImage(named: "bluearmynarikei")!, UIImage(named: "bluearmynarigin")!, UIImage(named: "bluearmykinsyou")!, UIImage(named: "bluearmyryuuma")!, UIImage(named: "bluearmyryuuou")!, UIImage(named: "bluearmyousyou")!]
    
    static let OrangeArmyImage: [UIImage] = [UIImage(named: "orangearmyhohei")!, UIImage(named: "orangearmykyousya")!, UIImage(named: "orangearmykeima")!, UIImage(named: "orangearmyginsyou")!, UIImage(named: "orangearmykinsyou")!, UIImage(named: "orangearmykaku")!, UIImage(named: "orangearmyhisya")!, UIImage(named: "orangearmyousyou")!]
    
    static let AdvanceOrangeArmyImage: [UIImage] = [UIImage(named: "orangearmytokin")!, UIImage(named: "orangearmynarika")!, UIImage(named: "orangearmynarikei")!, UIImage(named: "orangearmynarigin")!, UIImage(named: "orangearmykinsyou")!, UIImage(named: "orangearmyryuuma")!, UIImage(named: "orangearmyryuuou")!, UIImage(named: "orangearmyousyou")!]
    
    static let GrayArmyImage: [UIImage] = [UIImage(named: "grayarmyhohei")!, UIImage(named: "grayarmykyousya")!, UIImage(named: "grayarmykeima")!, UIImage(named: "grayarmyginsyou")!, UIImage(named: "grayarmykinsyou")!, UIImage(named: "grayarmykaku")!, UIImage(named: "grayarmyhisya")!, UIImage(named: "grayarmyousyou")!]
    
    static let AdvanceGrayArmyImage: [UIImage] = [UIImage(named: "grayarmytokin")!, UIImage(named: "grayarmynarika")!, UIImage(named: "grayarmynarikei")!, UIImage(named: "grayarmynarigin")!, UIImage(named: "grayarmykinsyou")!, UIImage(named: "grayarmyryuuma")!, UIImage(named: "grayarmyryuuou")!, UIImage(named: "grayarmyousyou")!]
    
    static let GoldArmyImage: [UIImage] = [UIImage(named: "goldarmyhohei")!, UIImage(named: "goldarmykyousya")!, UIImage(named: "goldarmykeima")!, UIImage(named: "goldarmyginsyou")!, UIImage(named: "goldarmykinsyou")!, UIImage(named: "goldarmykaku")!, UIImage(named: "goldarmyhisya")!, UIImage(named: "goldarmyousyou")!]
    
    static let AdvanceGoldArmyImage: [UIImage] = [UIImage(named: "goldarmytokin")!, UIImage(named: "goldarmynarika")!, UIImage(named: "goldarmynarikei")!, UIImage(named: "goldarmynarigin")!, UIImage(named: "goldarmykinsyou")!, UIImage(named: "goldarmyryuuma")!, UIImage(named: "goldarmyryuuou")!, UIImage(named: "goldarmyousyou")!]
    
    static let HadesArmyImage: [UIImage] = [UIImage(named: "hadesarmyhohei")!, UIImage(named: "hadesarmykyousya")!, UIImage(named: "hadesarmykeima")!, UIImage(named: "hadesarmyginsyou")!, UIImage(named: "hadesarmykinsyou")!, UIImage(named: "hadesarmykaku")!, UIImage(named: "hadesarmyhisya")!, UIImage(named: "hadesarmyousyou")!]
    
    static let AdvanceHadesArmyImage: [UIImage] = [UIImage(named: "hadesarmytokin")!, UIImage(named: "hadesarmynarika")!, UIImage(named: "hadesarmynarikei")!, UIImage(named: "hadesarmynarigin")!, UIImage(named: "hadesarmykinsyou")!, UIImage(named: "hadesarmyryuuma")!, UIImage(named: "hadesarmyryuuou")!, UIImage(named: "hadesarmyousyou")!]
    
    static let GreenArmyImage: [UIImage] = [UIImage(named: "greenarmyhohei")!, UIImage(named: "greenarmykyousya")!, UIImage(named: "greenarmykeima")!, UIImage(named: "greenarmyginsyou")!, UIImage(named: "greenarmykinsyou")!, UIImage(named: "greenarmykaku")!, UIImage(named: "greenarmyhisya")!, UIImage(named: "greenarmyousyou")!]
    
    static let AdvanceGreenArmyImage: [UIImage] = [UIImage(named: "greenarmytokin")!, UIImage(named: "greenarmynarika")!, UIImage(named: "greenarmynarikei")!, UIImage(named: "greenarmynarigin")!, UIImage(named: "greenarmykinsyou")!, UIImage(named: "greenarmyryuuma")!, UIImage(named: "greenarmyryuuou")!, UIImage(named: "greenarmyousyou")!]
    
    static let FireImage: [UIImage] = [UIImage(named: "firehohei")!, UIImage(named: "firekyousya")!, UIImage(named: "firekeima")!, UIImage(named: "fireginsyou")!, UIImage(named: "firekinsyou")!, UIImage(named: "firekaku")!, UIImage(named: "firehisya")!, UIImage(named: "fireousyou")!]
    
    static let AdvanceFireImage: [UIImage] = [UIImage(named: "firetokin")!, UIImage(named: "firenarika")!, UIImage(named: "firenarikei")!, UIImage(named: "firenarigin")!, UIImage(named: "firekinsyou")!, UIImage(named: "fireryuuma")!, UIImage(named: "fireryuuou")!, UIImage(named: "fireousyou")!]
    
    static let BlueFireImage: [UIImage] = [UIImage(named: "bluefirehohei")!, UIImage(named: "bluefirekyousya")!, UIImage(named: "bluefirekeima")!, UIImage(named: "bluefireginsyou")!, UIImage(named: "bluefirekinsyou")!, UIImage(named: "bluefirekaku")!, UIImage(named: "bluefirehisya")!, UIImage(named: "bluefireousyou")!]
    
    static let AdvanceBlueFireImage: [UIImage] = [UIImage(named: "bluefiretokin")!, UIImage(named: "bluefirenarika")!, UIImage(named: "bluefirenarikei")!, UIImage(named: "bluefirenarigin")!, UIImage(named: "bluefirekinsyou")!, UIImage(named: "bluefireryuuma")!, UIImage(named: "bluefireryuuou")!, UIImage(named: "bluefireousyou")!]
    
    static let OrangeFireImage: [UIImage] = [UIImage(named: "orangefirehohei")!, UIImage(named: "orangefirekyousya")!, UIImage(named: "orangefirekeima")!, UIImage(named: "orangefireginsyou")!, UIImage(named: "orangefirekinsyou")!, UIImage(named: "orangefirekaku")!, UIImage(named: "orangefirehisya")!, UIImage(named: "orangefireousyou")!]
    
    static let AdvanceOrangeFireImage: [UIImage] = [UIImage(named: "orangefiretokin")!, UIImage(named: "orangefirenarika")!, UIImage(named: "orangefirenarikei")!, UIImage(named: "orangefirenarigin")!, UIImage(named: "orangefirekinsyou")!, UIImage(named: "orangefireryuuma")!, UIImage(named: "orangefireryuuou")!, UIImage(named: "orangefireousyou")!]
    
    static let GrayFireImage: [UIImage] = [UIImage(named: "grayfirehohei")!, UIImage(named: "grayfirekyousya")!, UIImage(named: "grayfirekeima")!, UIImage(named: "grayfireginsyou")!, UIImage(named: "grayfirekinsyou")!, UIImage(named: "grayfirekaku")!, UIImage(named: "grayfirehisya")!, UIImage(named: "grayfireousyou")!]
    
    static let AdvanceGrayFireImage: [UIImage] = [UIImage(named: "grayfiretokin")!, UIImage(named: "grayfirenarika")!, UIImage(named: "grayfirenarikei")!, UIImage(named: "grayfirenarigin")!, UIImage(named: "grayfirekinsyou")!, UIImage(named: "grayfireryuuma")!, UIImage(named: "grayfireryuuou")!, UIImage(named: "grayfireousyou")!]
    
    static let GoldFireImage: [UIImage] = [UIImage(named: "goldfirehohei")!, UIImage(named: "goldfirekyousya")!, UIImage(named: "goldfirekeima")!, UIImage(named: "goldfireginsyou")!, UIImage(named: "goldfirekinsyou")!, UIImage(named: "goldfirekaku")!, UIImage(named: "goldfirehisya")!, UIImage(named: "goldfireousyou")!]
    
    static let AdvanceGoldFireImage: [UIImage] = [UIImage(named: "goldfiretokin")!, UIImage(named: "goldfirenarika")!, UIImage(named: "goldfirenarikei")!, UIImage(named: "goldfirenarigin")!, UIImage(named: "goldfirekinsyou")!, UIImage(named: "goldfireryuuma")!, UIImage(named: "goldfireryuuou")!, UIImage(named: "goldfireousyou")!]
    
    static let HadesFireImage: [UIImage] = [UIImage(named: "hadeshohei")!, UIImage(named: "hadeskyousya")!, UIImage(named: "hadeskeima")!, UIImage(named: "hadesginsyou")!, UIImage(named: "hadeskinsyou")!, UIImage(named: "hadeskaku")!, UIImage(named: "hadeshisya")!, UIImage(named: "hadesousyou")!]
    
    static let AdvanceHadesFireImage: [UIImage] = [UIImage(named: "hadestokin")!, UIImage(named: "hadesnarika")!, UIImage(named: "hadesnarikei")!, UIImage(named: "hadesnarigin")!, UIImage(named: "hadeskinsyou")!, UIImage(named: "hadesryuuma")!, UIImage(named: "hadesryuuou")!, UIImage(named: "hadesousyou")!]
    
    static let GreenFireImage: [UIImage] = [UIImage(named: "greenfirehohei")!, UIImage(named: "greenfirekyousya")!, UIImage(named: "greenfirekeima")!, UIImage(named: "greenfireginsyou")!, UIImage(named: "greenfirekinsyou")!, UIImage(named: "greenfirekaku")!, UIImage(named: "greenfirehisya")!, UIImage(named: "greenfireousyou")!]
    
    static let AdvanceGreenFireImage: [UIImage] = [UIImage(named: "greenfiretokin")!, UIImage(named: "greenfirenarika")!, UIImage(named: "greenfirenarikei")!, UIImage(named: "greenfirenarigin")!, UIImage(named: "greenfirekinsyou")!, UIImage(named: "greenfireryuuma")!, UIImage(named: "greenfireryuuou")!, UIImage(named: "greenfireousyou")!]
    
    /************  駒情報終わり   **************/
    //配列を配列に格納  同期に使う
    static let NoAdv:[[UIImage]] = [NormalImage, RedImage, OrangeImage, YellowImage, GreenImage, BlueImage, IndigoImage, PurpleImage, PinkImage, BlackImage, WhiteImage, SilverImage, GoldImage, ArmyImage, BlueArmyImage,
        OrangeArmyImage, GrayArmyImage, GoldArmyImage, HadesArmyImage, GreenArmyImage, FireImage,BlueFireImage,
        OrangeFireImage, GrayFireImage, GoldFireImage, HadesFireImage, GreenFireImage]
    
    static let Adv:[[UIImage]] = [AdvanceNormalImage, AdvanceRedImage, AdvanceOrangeImage, AdvanceYellowImage, AdvanceGreenImage, AdvanceBlueImage, AdvanceIndigoImage, AdvancePurpleImage, AdvancePinkImage, AdvanceBlackImage, AdvanceWhiteImage, AdvanceSilverImage, AdvanceGoldImage, AdvanceArmyImage, AdvanceBlueArmyImage,
        AdvanceOrangeArmyImage, AdvanceGrayArmyImage, AdvanceGoldArmyImage, AdvanceHadesArmyImage, AdvanceGreenArmyImage, AdvanceFireImage, AdvanceBlueFireImage,
        AdvanceOrangeFireImage, AdvanceGrayFireImage, AdvanceGoldFireImage, AdvanceHadesFireImage, AdvanceGreenFireImage]
    
}
