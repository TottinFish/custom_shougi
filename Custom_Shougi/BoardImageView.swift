//
//  BoardImageView.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/03.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class BoardImageView: UIView {
    let boardImage: [UIImage] = [UIImage(named: "wood0")!, UIImage(named: "wood1")!]
    let backImage = UIImage(named: "dragon1")
    
    override func layoutSubviews() {    //初期設定
        let CGWidth = self.frame.width
        let CGHeight = self.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        //print("backboard view test")
        
        super.layoutSubviews()
        let backImageView = UIImageView(image: backImage)
        //self.backgroundColor = UIColor.brown
        backImageView.frame = CGRect(x:0, y:0, width:CGWidth, height:CGHeight)
        self.addSubview(backImageView)
        
        //PlayBoardView.boardNumは外からも変えれるようにして、背景変える
        let boardView:UIImageView = UIImageView(image: boardImage[CustomizeBoardImageView.boardNum])
        //背景の追加
        boardView.frame = CGRect(x:sukima, y:miniHeight, width:miniWidth*9, height:miniWidth*9)
        self.addSubview(boardView)
    }
    
}
