//
//  CustomizePieceView.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/03.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

//駒が移動する時に呼び出される
//盤上と持ち駒両方を扱う(頻度の差はあるが，同じタイミングで再描画するため)

class CustomizePieceView: UIView {//将棋盤描写のクラス(下書き)
    var playView: UIViewController!
    var CGWidth: CGFloat!
    var CGHeight: CGFloat!
    var miniWidth: CGFloat!
    var miniHeight: CGFloat!
    var sukima: CGFloat!
    var PieceImage:[UIImage]!
    var AdvancePieceImage:[UIImage]!
    var SubPieceImage:[UIImage]!
    var SubAdvancePieceImage:[UIImage]!
    let reverse = CGFloat(Double.pi)
    
    func setInit(playView: UIViewController) { //持ち駒タップ時に上のviewに伝えるため保存
        self.playView = playView
    }
    
    //指定した持ち駒をラベルとともに描画
    func drawHavingPiece(labelNum: Int, isMe: Bool) {
        //個数用のラベル
        let label: UILabel = UILabel()
        label.removeFromSuperview()
        label.textColor = UIColor.black
        label.font = UIFont(name: "Piece Stock", size:sukima)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.3
        var imageView : UIImageView
        //駒の種類同期
        syncPiece()
        
        if(isMe) {
            label.frame = CGRect(x:sukima + miniWidth * CGFloat(labelNum) , y:CGHeight - miniHeight + sukima! + miniWidth, width:miniWidth, height:miniWidth)
            label.text = String(State.havingPieceNum1[labelNum])
        } else {
            label.frame = CGRect(x:CGWidth - sukima - (miniWidth * CGFloat(labelNum + 1)) , y:miniHeight - miniWidth - sukima! - miniWidth, width:miniWidth, height:miniWidth)
            label.text = String(State.havingPieceNum2[labelNum])
        }
        label.textAlignment = NSTextAlignment.center
        self.addSubview(label)
        
        //持ち駒の画像
        imageView = UIImageView(image: PieceImage[labelNum])
        if(isMe) {
            imageView.frame = CGRect(x:sukima + miniWidth * CGFloat(labelNum), y:CGHeight - miniHeight + sukima, width:miniWidth, height:miniWidth)
        } else {
            imageView.frame = CGRect(x:CGWidth - sukima - (miniWidth * CGFloat(labelNum + 1)) , y:miniHeight - miniWidth - sukima!, width:miniWidth, height:miniWidth)
            imageView.transform = CGAffineTransform(rotationAngle: reverse)
        }
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: playView, action: #selector(PageFor2Play.touchAction)))
        self.addSubview(imageView)
    }
    
    //盤上の駒を描画(nilはここでは想定していない)
    func drawBoardPiece(i: Int, j: Int) {
        let PositionX = (CGFloat((i + 1/2)) * miniWidth) + sukima
        let PositionY = (CGFloat((j + 1/2)) * miniWidth) + miniHeight
        var imageView : UIImageView
        //駒の種類同期
        syncPiece()
        
        if(State.boardState[i][j]!.state){
            imageView = UIImageView(image: PieceImage[State.boardState[i][j]!.kind])
        }else{
            imageView = UIImageView(image: AdvancePieceImage[State.boardState[i][j]!.kind])
        }
        imageView.frame = CGRect(x:PositionX , y:PositionY, width:miniWidth, height:miniWidth)
        if (State.boardState[i][j]?.own == false) {
            imageView.transform = CGAffineTransform(rotationAngle: reverse)
        }
        self.addSubview(imageView)
        
    }
    
    override func draw(_ rect: CGRect){//TODO 広告を入れるとか、メニュー欄の追加とか考えると再度微調整がいると思う
        CGWidth = self.frame.width
        CGHeight = self.frame.height
        miniWidth = CGWidth/10 //1マス分の1辺の長さ
        miniHeight = (CGHeight - (miniWidth * 9))/2
        sukima = CGWidth/20
        
        //盤面上の駒の描写
        for i in 0..<9 {
            for j in 0..<9 {
                if(State.boardState[i][j] != nil) {
                    drawBoardPiece(i: i, j: j)
                }
            }
        }
        
    }
    
    func syncPiece(){   //駒の種類の同期
        PieceImage = ImageData.NoAdv[UserInfo.setPiece]
        AdvancePieceImage = ImageData.Adv[UserInfo.setPiece]
        SubPieceImage = ImageData.NoAdv[UserInfo.SubsetPiece]
        SubAdvancePieceImage = ImageData.Adv[UserInfo.SubsetPiece]
    }
    
}
