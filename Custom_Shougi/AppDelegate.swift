//
//  AppDelegate.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit
//import NCMB
//import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    //********** APIキーの設定 **********
   /* let applicationkey = "8383b062a7c98c45517a08d2d84f37d7466fcf5d76c5d68458d8369eaf78c093"
    let clientkey      = "770c0134c337b1e3453ab6dfe868c6e77dec6076f46ba9621fe85048920994cb"*/

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //FirebaseApp.configure()//For firebase
        
        //最初の画面を指定
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = PageForTitle()
        window?.makeKeyAndVisible()
        
        // Override point for customization after application launch.
        //********** SDKの初期化 **********
        //NCMB.setApplicationKey(applicationkey, clientKey: clientkey)
        
        //********** データストアにデータを保存 **********
        /*let obj = NCMBObject(className: "TestClass")
        obj?.setObject("Hello, NCMB!", forKey: "message")
        obj?.saveInBackground({ (error) in
            if error != nil {
                // 保存に失敗した場合の処理
                print("save failed (T_T) ")
            }else{
                // 保存に成功した場合の処理
                print("save succeeded (^_^) ")
            }
        })*/
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

