//
//  LastMovedPiece.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/02/23.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class LastMovedPiece: UIView {
    //移動可能場所に印
    override func draw(_ rect: CGRect){
        let CGWidth = self.frame.width
        let CGHeight = self.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        let i = Advance.LastMoveTaple.0
        let j = Advance.LastMoveTaple.1
        let PositionX = (CGFloat((i + 1/2)) * miniWidth) + sukima
        let PositionY = (CGFloat((j + 1/2)) * miniWidth) + miniHeight
        
        var mySquare = UIBezierPath()   //グラフの点
        //let squarePoint = CGPoint(x:PositionX, y:PositionY)
        mySquare = UIBezierPath(roundedRect: CGRect(x: PositionX, y: PositionY, width: miniWidth, height: miniWidth), cornerRadius: 0)
        UIColor.red.setFill()
        mySquare.fill()
        mySquare.stroke()
                
       
    }
    
}
