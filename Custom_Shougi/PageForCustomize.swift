//
//  PageForCustomize.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

class PageForCustomize: UIViewController {
    let configs = UserDefaults.standard //保存データ
    let returnButton = UIButton()   //戻るボタン
    let titleLabel = UILabel()  //タイトルラベル
    let selectButtonFor1P = UIButton()  //1Pのカスタムの選択ボタン
    let selectButtonFor2P = UIButton()  //2Pのカスタムの選択ボタン
    let selectButtonForType1 = UIButton()  //タイプ1の選択ボタン
    let selectButtonForType2 = UIButton()  //タイプ2の選択ボタン
    let selectButtonForType3 = UIButton()  //タイプ3の選択ボタン
    let saveButton = UIButton()  //保存ボタン
    let transPageButtonForPiece = UIButton()  //駒の種類を変更するページに移動
    var board: CustomizeBoardView!   //盤
    var backboard: CustomizeBoardImageView!  //背景と盤の画像
    var pieceView: CustomizePieceView!   //駒
    var placeView: MovePlaceView!   //移動可能場所
    
    var isMe: Bool = true
    var type: Int = 1
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchAction))) //タッチ時のアクション(駒の移動)を指定
        
        //1Pのtypeをロード
        type = userDefaults.integer(forKey: "1PPlace")
        
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 1.0, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        
        //1Pボタン
        selectButtonFor1P.frame = CGRect(x:screenWidth * 4/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        selectButtonFor1P.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectButtonFor1P.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        selectButtonFor1P.layer.borderWidth = 1.0
        selectButtonFor1P.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        selectButtonFor1P.layer.cornerRadius = 25
        selectButtonFor1P.layer.shadowOpacity = 0.5
        selectButtonFor1P.layer.shadowOffset = CGSize(width: 2, height: 2)
        selectButtonFor1P.setTitle("1P", for: .normal)
        selectButtonFor1P.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        selectButtonFor1P.addTarget(self, action: #selector(self.select1Action(sender:)), for: .touchUpInside)
        
        //2Pボタン
        selectButtonFor2P.frame = CGRect(x:screenWidth * 7/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        selectButtonFor2P.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectButtonFor2P.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        selectButtonFor2P.layer.borderWidth = 1.0
        selectButtonFor2P.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        selectButtonFor2P.layer.cornerRadius = 25
        selectButtonFor2P.layer.shadowOpacity = 0.5
        selectButtonFor2P.layer.shadowOffset = CGSize(width: 2, height: 2)
        selectButtonFor2P.setTitle("2P", for: .normal)
        selectButtonFor2P.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        selectButtonFor2P.addTarget(self, action: #selector(self.select2Action(sender:)), for: .touchUpInside)
        
        //タイプ1のボタン
        selectButtonForType1.frame = CGRect(x:screenWidth * 1/10, y:screenHeight * 18/20, width:screenWidth/5, height: screenHeight * 2/40)
        selectButtonForType1.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectButtonForType1.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        selectButtonForType1.layer.borderWidth = 1.0
        selectButtonForType1.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        selectButtonForType1.layer.cornerRadius = 25
        selectButtonForType1.layer.shadowOpacity = 0.5
        selectButtonForType1.layer.shadowOffset = CGSize(width: 2, height: 2)
        selectButtonForType1.setTitle("タイプ1", for: .normal)
        selectButtonForType1.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        selectButtonForType1.addTarget(self, action: #selector(self.type1Action(sender:)), for: .touchUpInside)
        
        //タイプ2のボタン
        selectButtonForType2.frame = CGRect(x:screenWidth * 4/10, y:screenHeight * 18/20, width:screenWidth/5, height: screenHeight * 2/40)
        selectButtonForType2.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectButtonForType2.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        selectButtonForType2.layer.borderWidth = 1.0
        selectButtonForType2.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        selectButtonForType2.layer.cornerRadius = 25
        selectButtonForType2.layer.shadowOpacity = 0.5
        selectButtonForType2.layer.shadowOffset = CGSize(width: 2, height: 2)
        selectButtonForType2.setTitle("タイプ2", for: .normal)
        selectButtonForType2.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        selectButtonForType2.addTarget(self, action: #selector(self.type2Action(sender:)), for: .touchUpInside)
        
        //タイプ3のボタン
        selectButtonForType3.frame = CGRect(x:screenWidth * 7/10, y:screenHeight * 18/20, width:screenWidth/5, height: screenHeight * 2/40)
        selectButtonForType3.setTitleColor(UIColor.white, for: UIControl.State.normal)
        selectButtonForType3.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        selectButtonForType3.layer.borderWidth = 1.0
        selectButtonForType3.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        selectButtonForType3.layer.cornerRadius = 25
        selectButtonForType3.layer.shadowOpacity = 0.5
        selectButtonForType3.layer.shadowOffset = CGSize(width: 2, height: 2)
        selectButtonForType3.setTitle("タイプ3", for: .normal)
        selectButtonForType3.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        selectButtonForType3.addTarget(self, action: #selector(self.type3Action(sender:)), for: .touchUpInside)
        
        //保存ボタン
        saveButton.frame = CGRect(x:screenWidth * 4/10, y:screenHeight * 16/20, width:screenWidth/5, height: screenHeight * 2/40)
        saveButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        saveButton.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        saveButton.layer.borderWidth = 1.0
        saveButton.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        saveButton.layer.cornerRadius = 25
        saveButton.layer.shadowOpacity = 0.5
        saveButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        saveButton.setTitle("保存", for: .normal)
        saveButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        saveButton.addTarget(self, action: #selector(self.saveAction(sender:)), for: .touchUpInside)
        
        //駒選択ページ遷移ボタン
        transPageButtonForPiece.frame = CGRect(x:screenWidth * 7/10, y:screenHeight * 16/20, width:screenWidth/5, height: screenHeight * 2/40)
        transPageButtonForPiece.setTitleColor(UIColor.white, for: UIControl.State.normal)
        transPageButtonForPiece.backgroundColor = UIColor(red: 0.7, green: 0.1, blue: 0.1, alpha: 1)
        transPageButtonForPiece.layer.borderWidth = 1.0
        transPageButtonForPiece.layer.borderColor = UIColor(red: 0.7, green: 0.1, blue: 0.1, alpha: 1).cgColor
        transPageButtonForPiece.layer.cornerRadius = 25
        transPageButtonForPiece.layer.shadowOpacity = 0.5
        transPageButtonForPiece.layer.shadowOffset = CGSize(width: 2, height: 2)
        transPageButtonForPiece.setTitle("駒の選択", for: .normal)
        transPageButtonForPiece.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        transPageButtonForPiece.addTarget(self, action: #selector(self.selectPieceAction(sender:)), for: .touchUpInside)
        
        //背景と盤の画像の描写
        backboard = CustomizeBoardImageView()
        backboard.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(backboard)
        
        //盤の描画
        board = CustomizeBoardView()
        board.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(board)

         //駒の準備
        drawPiece()
        makePieceView()
        
        //BGM選択
        BGMmanager.select = 2
        BGMmanager.PlaySound()
        
        //ボタンの設置
        drawButtonAgain()
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
        removePieceView()
        //駒の準備
        drawPiece()
        makePieceView()
    }
    
    func drawButtonAgain() {
        returnButton.removeFromSuperview()
        selectButtonFor1P.removeFromSuperview()
        selectButtonFor2P.removeFromSuperview()
        selectButtonForType1.removeFromSuperview()
        selectButtonForType2.removeFromSuperview()
        selectButtonForType3.removeFromSuperview()
        saveButton.removeFromSuperview()
        transPageButtonForPiece.removeFromSuperview()
        if(isMe) {
            selectButtonFor1P.backgroundColor = UIColor.red
            selectButtonFor2P.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        } else {
            selectButtonFor1P.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
            selectButtonFor2P.backgroundColor = UIColor.red
        }
        if(type == 1) {
            selectButtonForType1.backgroundColor = UIColor.red
            selectButtonForType2.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
            selectButtonForType3.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        } else if(type == 2) {
            selectButtonForType1.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
            selectButtonForType2.backgroundColor = UIColor.red
            selectButtonForType3.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        } else {
            selectButtonForType1.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
            selectButtonForType2.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
            selectButtonForType3.backgroundColor = UIColor.red
        }
        view.addSubview(returnButton) //戻るボタン設置
        view.addSubview(selectButtonFor1P) //1Pボタン設置
        view.addSubview(selectButtonFor2P) //2Pボタン設置
        view.addSubview(selectButtonForType1) //タイプ1のボタン設置
        view.addSubview(selectButtonForType2) //タイプ2のボタン設置
        view.addSubview(selectButtonForType3) //タイプ3のボタン設置
        view.addSubview(saveButton) //保存ボタン設置
        view.addSubview(transPageButtonForPiece) //駒選択ボタン設置
    }
    
    //駒のロード
    func drawPiece() {
        for x in 0..<9 {
            for y in 0..<9 {
                State.boardState[x][y] = nil
            }
        }
        FileController.readPieceData(isMe: isMe, type: type)   //駒のロード
    }
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {
        BGMmanager.StopSound()
        BGMmanager.select = 0
        BGMmanager.PlaySound()
        self.dismiss(animated: true, completion: nil)
    }
    
    //保存ボタンの挙動
    @objc func saveAction(sender: UIButton) {
        FileController.writePieceData(isMe: isMe, type: type)
        if(isMe) {
            userDefaults.set(type, forKey: "1PPlace")
        } else {
            userDefaults.set(type, forKey: "2PPlace")
        }
    }
    
    //駒選択ボタンの挙動
    @objc func selectPieceAction(sender: UIButton) {
        let pagePieceSelect = PageForPieceSelect()//ページ用意
        self.present(pagePieceSelect, animated: true, completion: nil) //ページ遷移
    }
    
    //ロードと再描画
    func buttonAction() {
        if(placeView != nil) {  //前のviewを削除(最初と範囲外指定後はnil)
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
        }
        pieceView.removeFromSuperview()
        makePieceView()
        Advance.isTouched = false    //状態変更
        drawPiece()
        drawButtonAgain()
    }
    
    //1Pボタンの挙動
    @objc func select1Action(sender: UIButton) {
        isMe = true
        type = userDefaults.integer(forKey: "1PPlace")
        buttonAction()
    }
    
    //2Pボタンの挙動
    @objc func select2Action(sender: UIButton) {
        isMe = false
        type = userDefaults.integer(forKey: "2PPlace")
        buttonAction()
    }
    
    //type1ボタンの挙動
    @objc func type1Action(sender: UIButton) {
        type = 1
        buttonAction()
    }
    
    //type2ボタンの挙動
    @objc func type2Action(sender: UIButton) {
        type = 2
        buttonAction()
    }
    
    //type3ボタンの挙動
    @objc func type3Action(sender: UIButton) {
        type = 3
        buttonAction()
    }
    
    //subviewを全て削除
    func removeAllSubviews(parentView: UIView){
        let subviews = parentView.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func makePieceView() {
        pieceView = CustomizePieceView()
        pieceView.setInit(playView: self)
        pieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        pieceView.backgroundColor = UIColor.clear
        view.addSubview(pieceView)
    }
    
    
    //タッチ時に呼び出されるメソッド
    @objc func touchAction(sender: UITapGestureRecognizer) {
        let CGWidth = self.view.frame.width
        let CGHeight = self.view.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        let tapPoint = sender.location(in: self.view)

        if(placeView != nil) {  //前のviewを削除(最初と範囲外指定後はnil)
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
        }
        
        //盤での位置を計算
        var touchedX = Int((tapPoint.x - sukima)/miniWidth)
        var touchedY = Int((tapPoint.y - miniHeight)/miniWidth)
        
        if((tapPoint.x - sukima) < 0 ) {
            touchedX = -1
        }else if(miniHeight > tapPoint.y && tapPoint.y > miniHeight - sukima ){
            touchedY = -1
        }
        
        //駒を移動
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && Advance.isAbleToMove(i: touchedX, j: touchedY)) {
            CustomizeAdvance.move(i: touchedX, j: touchedY)  //駒の移動(データ)
            //再描画
            pieceView.removeFromSuperview()
            makePieceView()
            Advance.isTouched = false    //状態変更
            drawButtonAgain()
            return
        }
        
        //移動可能場所を表示するview
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && State.boardState[touchedX][touchedY] != nil){    //駒がある時
            Advance.isTouched = true    //状態変更
            CustomizeAdvance.placeCandidate(i: touchedX, j: touchedY, isMe: isMe)  //移動可能場所を探す(placeに保存される)
            makePlaceView()
        } else if(placeView != nil) {   //範囲外指定を連続でした時は既にnilなので
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
            placeView = nil
            Advance.isTouched = false    //状態変更
        }
        
        drawButtonAgain()
    }
    
    func makePlaceView() {
        placeView = MovePlaceView()
        placeView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        placeView.backgroundColor = UIColor.clear
        placeView.alpha = 0.5
        self.view.addSubview(placeView)
    }
    
    func removePieceView(){//前の駒が重なるので
        if(pieceView != nil){
            pieceView.removeFromSuperview()
        }
    }
    
}

