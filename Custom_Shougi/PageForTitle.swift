//
//  PageForTitle.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit
import AVFoundation
import SpriteKit

class PageForTitle: UIViewController {
    let titleLabel = UILabel()  //タイトルラベル
    let play2 = UIButton()  //2play用スタートボタン
    let option = UIButton() //OPtion用スタートボタン
    let customize = UIButton() //Customize用スタートボタン
    let connect = UIButton() //Connect用スタートボタン
    let online = UIButton() //OnlinePlay用スタートボタン
    var backboard: TitleBackView!  //背景の画像
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        //ユーザーデフォルト初期値設定
        let userDefaults = UserDefaults.standard
        userDefaults.register(defaults: ["1PPlace" : 0,
                                         "2PPlace" : 0,
                                         "userName": "未設定"])

        //SKViewを取得する。
        /*let skView = self.view as! SKView
        
        //SKSファイルを指定してSKSceneインスタンスを生成する。
        let scene = TitleScene(fileNamed: "TitleScene")
        
        //現在シーンを設定する。
        skView.presentScene(scene)*/
        
        makeBackView()
        
        //タイトルラベル
        /*titleLabel.text = "カスタム将棋(仮)"
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HiraKakuProN-W3", size:50)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.3
        titleLabel.frame = CGRect(x:screenWidth/4, y:screenHeight * 4/20 ,width:screenWidth/2, height:screenHeight * 1/10)
        titleLabel.textAlignment = NSTextAlignment.center
        view.addSubview(titleLabel)*/
        
        //Connect用スタートボタン
        connect.frame = CGRect(x:screenWidth/4, y:screenHeight * 20/40, width:screenWidth/2, height: screenHeight * 2/40)
        connect.setTitleColor(UIColor.white, for: UIControl.State.normal)
        connect.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        connect.layer.borderWidth = 1.0
        connect.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        connect.layer.cornerRadius = 25
        connect.layer.shadowOpacity = 0.5
        connect.layer.shadowOffset = CGSize(width: 2, height: 2)
        connect.setTitle("OnlinePlay", for: .normal)
        //connect.setTitle("Start", for: UIControlState.highlighted)  //(後から追加しよう)
        connect.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        connect.addTarget(self, action: #selector(self.jumpConnect(sender:)), for: .touchUpInside)
        view.addSubview(connect)
        
        //2play用スタートボタン
        play2.frame = CGRect(x:screenWidth/4, y:screenHeight * 24/40, width:screenWidth/2, height: screenHeight * 2/40)
        play2.setTitleColor(UIColor.white, for: UIControl.State.normal)
        play2.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        play2.layer.borderWidth = 1.0
        play2.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        play2.layer.cornerRadius = 25
        play2.layer.shadowOpacity = 0.5
        play2.layer.shadowOffset = CGSize(width: 2, height: 2)
        play2.setTitle("2Play", for: .normal)
        //play2.setTitle("Start", for: UIControlState.highlighted)  //(後から追加しよう)
        play2.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        play2.addTarget(self, action: #selector(self.jump2Play(sender:)), for: .touchUpInside)
        view.addSubview(play2)
        
        //Customize用スタートボタン
        customize.frame = CGRect(x:screenWidth/4, y:screenHeight * 28/40, width:screenWidth/2, height: screenHeight * 2/40)
        customize.setTitleColor(UIColor.white, for: UIControl.State.normal)
        customize.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        customize.layer.borderWidth = 1.0
        customize.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        customize.layer.cornerRadius = 25
        customize.layer.shadowOpacity = 0.5
        customize.layer.shadowOffset = CGSize(width: 2, height: 2)
        customize.setTitle("Customize", for: .normal)
        //customize.setTitle("Start", for: UIControlState.highlighted)  //(後から追加しよう)
        customize.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        customize.addTarget(self, action: #selector(self.jumpCustom(sender:)), for: .touchUpInside)
        view.addSubview(customize)
        
        //Option用スタートボタン
        option.frame = CGRect(x:screenWidth/4, y:screenHeight * 32/40, width:screenWidth/2, height: screenHeight * 2/40)
        option.setTitleColor(UIColor.white, for: UIControl.State.normal)
        option.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        option.layer.borderWidth = 1.0
        option.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        option.layer.cornerRadius = 25
        option.layer.shadowOpacity = 0.5
        option.layer.shadowOffset = CGSize(width: 2, height: 2)
        option.setTitle("Option", for: .normal)
        //option.setTitle("Start", for: UIControlState.highlighted)  //(後から追加しよう)
        option.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        option.addTarget(self, action: #selector(self.jumpOption(sender:)), for: .touchUpInside)
        view.addSubview(option)
    
        //BGM再生
        BGMmanager.select = 3//Bgm選択
        BGMmanager.PlaySound()//うるさければコメントアウト
        
        //盤の初期化
        for x in 0..<9 {
            for y in 0..<9 {
                State.boardState[x][y] = nil
            }
        }
        State.initState()
        Advance.ResetAdvance()  //checkmateとかの初期化
        
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
        play2.isEnabled = true
        option.isEnabled = true
        customize.isEnabled = true
        connect.isEnabled = true
        //BGM再生
        BGMmanager.select = 0//Bgm選択
        BGMmanager.PlaySound()//うるさければコメントアウト
        if(isConnecting && !wantToClose) {  //画面閉じたとか
            print("タイトル画面でclose")
            isConnecting = false
            inputStream.close()
            outputStream.close()
        }
    }
    
    func makeBackView() {
        backboard = TitleBackView()
        backboard.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        backboard.backgroundColor = UIColor.clear
       // backboard.alpha = 0
        self.view.addSubview(backboard)
    }
    
    //記録ボタンのイベント
    @objc func jump2Play(sender: UIButton) {
        play2.isEnabled = false //連続押し防止(他のボタンもenableに)
        let nextpage2Play = PageFor2Play()   //ページ用意
        BGMmanager.StopSound()
        self.present(nextpage2Play, animated: true, completion: nil) //ページ遷移
    }
    
    @objc func jumpOption(sender: UIButton) {
        option.isEnabled = false//連続押し防止(他のボタンもenableに)
        let nextpageOption = PageForOption()//ページ用意
        self.present(nextpageOption, animated: true, completion: nil) //ページ遷移
    }
    
    @objc func jumpCustom(sender: UIButton) {
        customize.isEnabled = false//連続押し防止(他のボタンもenableに)
        let nextpageCustom = PageForCustomize()//ページ用意
        BGMmanager.StopSound()
        self.present(nextpageCustom, animated: true, completion: nil) //ページ遷移
    }
    
    @objc func jumpConnect(sender: UIButton) {
        if(!isObserving) {
            connect.isEnabled = false//連続押し防止(他のボタンもenableに)
            let nextpageConnect = PageForConnect()//ページ用意
            BGMmanager.StopSound()
            self.present(nextpageConnect, animated: true, completion: nil) //ページ遷移
        }
    }
}
