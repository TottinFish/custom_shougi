//
//  PageForMatching.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/26.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class PageForMatching: UIViewController {
    
    let OpNameLabel = UILabel()  //相手の名前ラベル
    let OpRateLabel = UILabel() //相手のレートラベル
    let userDefaults = UserDefaults.standard
    var isPageSift = false
    
    static var OpName = ""
    static var OpRate = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        OpNameLabel.text = "相手の名前: " + opponentName
        OpNameLabel.textColor = UIColor.black
        OpNameLabel.font = UIFont(name: "HiraKakuProN-W3", size:50)
        OpNameLabel.adjustsFontSizeToFitWidth = true
        OpNameLabel.minimumScaleFactor = 0.3
        OpNameLabel.frame = CGRect(x:screenWidth/4, y:screenHeight * 4/20 ,width:screenWidth/2, height:screenHeight * 1/10)
        OpNameLabel.textAlignment = NSTextAlignment.center
        view.addSubview(OpNameLabel)
        
        OpRateLabel.text = "相手のレート: " + opponentRate
        OpRateLabel.textColor = UIColor.black
        OpRateLabel.font = UIFont(name: "HiraKakuProN-W3", size:50)
        OpRateLabel.adjustsFontSizeToFitWidth = true
        OpRateLabel.minimumScaleFactor = 0.3
        OpRateLabel.frame = CGRect(x:screenWidth/4, y:screenHeight * 6/20 ,width:screenWidth/2, height:screenHeight * 1/10)
        OpRateLabel.textAlignment = NSTextAlignment.center
        view.addSubview(OpRateLabel)
        
        //非同期処理
        let queue = OperationQueue()
        queue.addOperation() {
            //盤の初期化
            for x in 0..<9 {
                for y in 0..<9 {
                    State.boardState[x][y] = nil
                }
            }
            //駒情報の送信
            Transmission().sendMessage(message: FileController.readPieceData(isMe: true,type: self.userDefaults.integer(forKey: "1PPlace")))
            
            queue.addOperation() {
                //駒情報を受け取る
                let receive = Transmission().listenAndRead(rootView: self)
                if(!isConnecting) {  //名前交換後にこちらが切断か,相手が切断したことを受信
                    return
                }
                State.setReceiveData(str: receive)  //相手の駒配置をセット
                connectState = 2
            }
            
            sleep(5)    //ユーザが表示を見やすくするため
            
            while(connectState == 1 && isConnecting){
                sleep(1)
            }
            
            OperationQueue.main.addOperation() {
                if(!isConnecting) { //相手が切断(window表示すべき?????)
                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                } else {
                    self.isPageSift = true
                    self.present(PageForOnlinePlay(), animated: true, completion: nil) //ページ遷移
                }
            }
        }
    }
    
    static func syncOpInfo(Name: String, Rate: String){
        OpName = Name
        OpRate = Rate
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if(!isPageSift) {
            if(isConnecting) {
                wantToClose = true
            }
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
}
