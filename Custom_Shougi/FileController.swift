//
//  FileController.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2019/02/12.
//  Copyright © 2019 Totty. All rights reserved.
//

import Foundation

class FileController {
    //改行は手動でしてからこのメソッドへ(isAdd壊れてる)
    static func writeFile(isAdd: Bool, string: String, fileName: String) {
        if let documentDirectoryFileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            //ファイルのフルパスを作る
            let targetFilePath = documentDirectoryFileURL.appendingPathComponent(fileName)
            do {
                if(isAdd) {  // ファイルの最後に追記
                    let fileHandle = try FileHandle(forWritingTo: targetFilePath)
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(string.data(using: String.Encoding.utf8)!)
                } else {
                    try string.write(to: targetFilePath, atomically: true, encoding: String.Encoding.utf8)
                }
            } catch let error as NSError {
                print("failed to append: \(error)")
            }
        }else{
            print("fileURLの取得に失敗")
        }
        
        //DocumentディレクトリのfileURLを取得
        /*if let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String? {
            //ファイルのフルパスを作る
            let targetFilePath = docDir + fileName
            print(targetFilePath)
            //ファイルに保存(上書き)
            do {
                try string.write(toFile: targetFilePath, atomically: true, encoding: String.Encoding.utf8)
            } catch let error as NSError {
                print("failed to write: \(error)")
            }
        }else{
            print("fileURLの取得に失敗")
        }*/
        
    }
    
    static func makeFileNum(isMe: Bool) -> String {
        if(isMe) {
            return "1P"
        } else {
            return "2P"
        }
    }
    //駒情報をファイルに書き込む(custom用)
    static func writePieceData(isMe: Bool, type: Int){
        writeFile(isAdd: false, string: mekeDataForWriteFile(isMe: isMe, isGame: false), fileName: "/pieceData\(makeFileNum(isMe: isMe))\(type).csv")
    }
    
    //指定した形式の盤データを作成
    static func mekeDataForWriteFile(isMe: Bool, isGame: Bool) -> String {
        var data: String = ""
        for x in 0..<9 {
            data += String(x)
            for y in 0..<9 {
                if(isGame) {
                    if(State.boardState[x][y] == nil) {
                        data += ",0"
                    } else if(State.boardState[x][y]!.own && State.boardState[x][y]!.state) {   //自軍,表
                        data += "," + String(State.boardState[x][y]!.kind + 1)
                    } else if(State.boardState[x][y]!.own) {    //自軍,裏
                        data += "," + String(State.boardState[x][y]!.kind + 101)
                    } else if(!State.boardState[x][y]!.own) {  //敵軍,表
                        data += "," + String((State.boardState[x][y]!.kind + 1) * -1)
                    } else {    //敵軍,裏
                        data += "," + String((State.boardState[x][y]!.kind + 101) * -1)
                    }
                } else if (State.boardState[x][y] != nil && State.boardState[x][y]!.own == isMe) {  //駒あり
                    data += "," + String(State.boardState[x][y]!.kind)
                } else {
                    data += ",-1"
                }
            }
            data += "\n"
        }
        return data
    }
    
    //駒情報を読み込む(ないならここで作る)(初期化はここではしない)
    static func readPieceData(isMe: Bool, type: Int) -> String{
        let fileName = "pieceData\(makeFileNum(isMe: isMe))\(type).csv"
        var stateTmp = ","
        //DocumentディレクトリのfileURLを取得
        if let documentDirectoryFileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            //ファイルのフルパスを作る
            let targetFilePath = documentDirectoryFileURL.appendingPathComponent(fileName)
            do{
                let csvData = try String(contentsOf: targetFilePath, encoding: String.Encoding.utf8)
                let dataList = csvData.components(separatedBy: "\n")
                
                //データをセット
                for x in 0..<9 {
                    let data = dataList[x].components(separatedBy: ",")
                    for y in 0..<9 {
                        if(data[y + 1] != "-1") {   //駒があるなら
                            State.boardState[x][y] = State.piece(own: isMe, kind: Int(data[y + 1]) ?? 0, state: true)
                        }
                        if(isConnecting && y > 4) {
                            //送信用駒データの用意
                            stateTmp += data[y + 1] + ","
                        }
                    }
                }
            }catch{ //ファイルがないので新規作成
                State.defaultPlace(isMe: isMe)
                writePieceData(isMe: isMe, type: type)
            }
        }else{
            print("fileURLの取得に失敗")
        }
        return stateTmp
    }
    
    static func saveState() {
        //盤面
        var data: String = mekeDataForWriteFile(isMe: false, isGame: true) + "1"
        //持ち駒
        for x in 0..<8 {
            data += "," + String(State.havingPieceNum1[x])
        }
        data += "\n2"
        for x in 0..<8 {
            data += "," + String(State.havingPieceNum2[x])
        }
        data += "\n"
        //履歴
        for x in 0..<State.history.count {
            data += String(x)
            for y in 0..<9 {
                data += "," + String(State.history[x][y])
            }
            data += "\n"
        }
        writeFile(isAdd: false, string: data, fileName: "history.csv")
    }
    
    //初期化もする
    static func loadState() {
        //盤の初期化
        for x in 0..<9 {
            for y in 0..<9 {
                State.boardState[x][y] = nil
            }
        }
        
        //DocumentディレクトリのfileURLを取得
        if let documentDirectoryFileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
            //ファイルのフルパスを作る
            let targetFilePath = documentDirectoryFileURL.appendingPathComponent("history.csv")
            do{
                let csvData = try String(contentsOf: targetFilePath, encoding: String.Encoding.utf8)
                let dataList = csvData.components(separatedBy: "\n")
                if(dataList.count < 11) {
                    return
                }
                //データをセット
                for x in 0..<9 {
                    let data = dataList[x].components(separatedBy: ",")
                    for y in 0..<9 {
                        let tmp = Int(data[y + 1]) ?? 0
                        //駒の種類+1が自軍,それに-1をかけたものが敵軍(裏なら1ではなく101)
                        if(tmp > 100) { //自軍,裏
                            State.boardState[x][y] = State.piece(own: true, kind: tmp - 101, state: false)
                        } else if(tmp > 0) {    //自軍,表
                            State.boardState[x][y] = State.piece(own: true, kind: tmp - 1, state: true)
                        } else if(tmp < -100) {  //敵軍,裏
                            State.boardState[x][y] = State.piece(own: false, kind: tmp * -1 - 101, state: false)
                        } else if(tmp < 0) {    //敵軍,表
                            State.boardState[x][y] = State.piece(own: false, kind: tmp * -1 - 1, state: true)
                        }
                    }
                }
                let data9 = dataList[9].components(separatedBy: ",")
                State.havingPieceNum1 = []
                for x in 1..<9 {
                    State.havingPieceNum1.append(Int(data9[x]) ?? 0)
                }
                let data10 = dataList[10].components(separatedBy: ",")
                State.havingPieceNum2 = []
                for x in 1..<9 {
                    State.havingPieceNum2.append(Int(data10[x]) ?? 0)
                }
                State.history = []
                for x in 11..<dataList.count - 1 {
                    let tmp = dataList[x].components(separatedBy: ",")
                    var historyTmp: [Int] = []
                    for y in 1..<10 {
                        historyTmp.append(Int(tmp[y]) ?? 0)
                    }
                    State.history.append(historyTmp)
                }
            }catch{ //ファイルがない(何もしなくて良い)
            }
        }else{
            print("fileURLの取得に失敗")
        }
    }
}
