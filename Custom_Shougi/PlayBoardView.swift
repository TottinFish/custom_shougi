//
//  View2Play.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/17.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//盤のマス目を表示するためのクラス
//駒は毎回再描画するが，マス目はしないほうが動作が軽くなるので別クラスを作成(背景もここ)

class PlayBoardView: UIView {//将棋盤描写のクラス(下書き)
    
    override func layoutSubviews() {    //初期設定
        super.layoutSubviews()
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect){//TODO 広告を入れるとか、メニュー欄の追加とか考えると再度微調整がいると思う
        let path = UIBezierPath()
        let CGWidth = self.frame.width
        let CGHeight = self.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        
        //盤の描画
        for i in 0..<10 {//縦ライン描写
            let tmpWidth = miniWidth * CGFloat(i)//intのiとCGFkoatのminiwidthのミスキャスト防止
            path.move(to: CGPoint(x:sukima + tmpWidth, y: miniHeight))
            path.addLine(to: CGPoint(x:sukima + tmpWidth, y: CGHeight - miniHeight))
        }
        for i in 0..<10{//横ライン描写
            let tmpHeight = miniWidth * CGFloat(i)//intのiとCGFkoatのminiwidthのミスキャスト防止
            path.move(to: CGPoint(x:sukima , y:miniHeight + tmpHeight))
            path.addLine(to: CGPoint(x:CGWidth - sukima, y:miniHeight + tmpHeight))
        }
        path.lineWidth = 2.5 // 線の太さ
        UIColor.black.setStroke() // 色をセット
        path.stroke()
        
    }
}
