//
//  SelectPieceVisibleView.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/14.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class SelectPieceVisibleView: UIView {
    
    //選択中の駒を赤色で囲む
    override func draw(_ rect: CGRect){
        let CGWidth = self.frame.width
        let CGHeight = self.frame.height
        let oneBlock = CGWidth/4
        let tmpX:Int = PageForPieceSelect.tmpX
        let tmpY:Int = PageForPieceSelect.tmpY
        
        if(tmpY < 0){
            return
        }
        var mySquare = UIBezierPath()   //グラフの点
        //let squarePoint = CGPoint(x:PositionX, y:PositionY)
        mySquare = UIBezierPath(roundedRect: CGRect(x: CGFloat(tmpX) * oneBlock , y:CGHeight/6 + CGFloat(tmpY) * oneBlock , width: oneBlock, height: oneBlock), cornerRadius: 0)
        UIColor.red.setStroke()
        mySquare.stroke()
    }
    
}
