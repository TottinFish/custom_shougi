//
//  PageForPieceSelect.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/06.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class PageForPieceSelect: UIViewController {
    let returnButton = UIButton()   //戻るボタン
    let prePageButton = UIButton()    //前のページボタン
    let postPageButton = UIButton()   //次のページボタン
    var pieceCount:Int = 0
    var pieceX:CGFloat = 0
    var pieceY:CGFloat = 0
    var skip: Int = 0
    var selectPieceView: SelectPieceView!   //駒
    var selectPieceVisibleView: SelectPieceVisibleView!   //駒
    static var tmpX: Int = 0
    static var tmpY: Int = 0
    var tmpsetPiece: Int!
    
    override func viewDidLoad() {
        
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchAction))) //タッチ時のアクション
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 1.0, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        
        prePageButton.frame = CGRect(x:screenWidth/10, y:screenHeight/10, width:screenWidth/5, height: screenHeight * 2/40)
        prePageButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        prePageButton.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1)
        prePageButton.layer.borderWidth = 1.0
        prePageButton.layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 1).cgColor
        prePageButton.layer.cornerRadius = 25
        prePageButton.layer.shadowOpacity = 0.5
        prePageButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        prePageButton.setTitle("前ページ", for: .normal)
        prePageButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        prePageButton.addTarget(self, action: #selector(self.prePageAction(sender:)), for: .touchUpInside)
        
        postPageButton.frame = CGRect(x:screenWidth * 7/10, y:screenHeight/10, width:screenWidth/5, height: screenHeight * 2/40)
        postPageButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        postPageButton.backgroundColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1)
        postPageButton.layer.borderWidth = 1.0
        postPageButton.layer.borderColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1).cgColor
        postPageButton.layer.cornerRadius = 25
        postPageButton.layer.shadowOpacity = 0.5
        postPageButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        postPageButton.setTitle("次ページ", for: .normal)
        postPageButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        postPageButton.addTarget(self, action: #selector(self.postPageAction(sender:)), for: .touchUpInside)
        
        
        if(selectPieceView != nil){
            selectPieceView.removeFromSuperview()
        }
        makeSelectPieceView()
        drawButtonAgain()
    }
    
    func drawButtonAgain() {
        returnButton.removeFromSuperview()
        view.addSubview(returnButton) //戻るボタン設置
        prePageButton.removeFromSuperview()
        view.addSubview(prePageButton) //前ページボタン設置
        postPageButton.removeFromSuperview()
        view.addSubview(postPageButton) //次ページボタン設置
    }
    
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func prePageAction(sender: UIButton) {
        if(SelectPieceView.piecePage > 0){
            SelectPieceView.piecePage -= 1
        }
        removeAllSubviews(parentView: selectPieceView)
        selectPieceView.removeFromSuperview()
        makeSelectPieceView()
        drawButtonAgain()
    }
    
    @objc func postPageAction(sender: UIButton) {
        let maxPage = UserInfo.PieceBool.count/20
        
        if(SelectPieceView.piecePage < maxPage){
            SelectPieceView.piecePage += 1
        }
        removeAllSubviews(parentView: selectPieceView)
        selectPieceView.removeFromSuperview()
        makeSelectPieceView()
        drawButtonAgain()
    }
    
    //タッチ時に呼び出されるメソッド
    @objc func touchAction(sender: UITapGestureRecognizer) {
        let CGWidth = self.view.frame.width
        let CGHeight = self.view.frame.height
        let oneBlock = CGWidth/4
        let tapPoint = sender.location(in: self.view)
        var add = 0
        tmpsetPiece = UserInfo.setPiece
        if(selectPieceView != nil){
            removeAllSubviews(parentView: selectPieceView)
            selectPieceView.removeFromSuperview()
        }
        
        //盤での位置を計算
        let touchedX = Int(tapPoint.x/oneBlock)
        var touchedY = Int((tapPoint.y - CGHeight/6)/oneBlock)
        
        PageForPieceSelect.tmpX = touchedX
        PageForPieceSelect.tmpY = touchedY
        
        if(touchedY > 4 || tapPoint.y < CGHeight/6){
            touchedY = -1
        }
        
        print("before changed setpiece is \(UserInfo.setPiece)")
        
        
        if(touchedY != -1){
            UserInfo.setPiece = 0
            UserInfo.setPiece += SelectPieceView.piecePage * 20
            UserInfo.setPiece += touchedX
            UserInfo.setPiece += touchedY * 4
            if(adjustCount(setNum: UserInfo.setPiece) > 0){
                add = adjustCount(setNum: UserInfo.setPiece)
                UserInfo.setPiece += adjustCount(setNum: UserInfo.setPiece + add)
            }
            
            if(UserInfo.setPiece >= UserInfo.PieceBool.count){//範囲外指定をしないため
                if(tmpsetPiece != nil){
                    UserInfo.setPiece = tmpsetPiece
                }
            }else{
                tmpsetPiece = UserInfo.setPiece
            }
            print("setpiece is \(UserInfo.setPiece)")
        }
        
        makeSelectPieceView()
        makeSelectPieceVisibleView()
        drawButtonAgain()
    }
    
    func makeSelectPieceView() {
        selectPieceView = SelectPieceView()
        selectPieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        selectPieceView.backgroundColor = UIColor.clear
        view.addSubview(selectPieceView)
    }
    
    func makeSelectPieceVisibleView() {
        if(selectPieceVisibleView != nil){
            selectPieceVisibleView.removeFromSuperview()
        }
        selectPieceVisibleView = SelectPieceVisibleView()
        selectPieceVisibleView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        selectPieceVisibleView.backgroundColor = UIColor.clear
        view.addSubview(selectPieceVisibleView)
    }
    
    //subviewを全て削除
    func removeAllSubviews(parentView: UIView){
        let subviews = parentView.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func adjustCount(setNum: Int)->Int{
        var adjC = 0
        
        if (setNum >= UserInfo.PieceBool.count) {
            return 0
        }
        
        for x in 0...setNum{
            if(!UserInfo.PieceBool[x]){
                adjC += 1
                print("setnum is \(setNum)")
            }
        }
        
        return adjC
    }
    
}
