//
//  Extensions.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2019/02/23.
//  Copyright © 2019 Totty. All rights reserved.
//

import Foundation

extension Int8 {
    var asUInt16: UInt16 {
        return UInt16(Int16(self) & 0xff)
    }
}

extension UInt16 {
    var asInt8: Int8 {
        let v = self & 0xff
        return Int8(v >> 4) << 4 | Int8(v & 0xf)
    }
    var asUInt32: UInt32 {
        return UInt32(UInt32(self) & 0xffff)
    }
}

extension Int32 {
    var asInt: Int {
        return Int(Int(self) & 0xffff)
    }
}

extension sockaddr {
    init(_ saddr_in: sockaddr_in) { // cast(copy) from sockaddr_in
        self = saddr_in.copyAsSockAddr()
    }
    
    func copyAsSockAddrIn() -> sockaddr_in {
        let len = UInt8(MemoryLayout<sockaddr_in>.size)
        let family = self.sa_family
        let (hi, lo) = (self.sa_data.0, self.sa_data.1)
        let port = (hi.asUInt16 << 8) | lo.asUInt16       // what's asUInt16?
        let b = (UInt32(Int16(self.sa_data.2) & 0x00ff), UInt32(Int16(self.sa_data.3) & 0x00ff),
                 UInt32(Int16(self.sa_data.4) & 0x00ff), UInt32(Int16(self.sa_data.5) & 0x00ff))
        let sadr =  b.0 << 24 | b.1 << 16 | b.2 << 8 | b.3
        let addr = in_addr(s_addr: sadr)
        
        return sockaddr_in(sin_len: len, sin_family: family, sin_port: port,
                           sin_addr: addr, sin_zero: (0,0,0,0,0,0,0,0))
    }
    
    func unsafeCopyAsSockAddrIn() -> sockaddr_in {
        return unsafeBitCast(self, to: sockaddr_in.self)
    }
}

extension sockaddr_in {
    init(_ saddr: sockaddr) {
        self = saddr.copyAsSockAddrIn()
    }
    
    func copyAsSockAddr() -> sockaddr {
        let len = self.sin_len
        let family = self.sin_family
        let port = self.sin_port.bigEndian
        let (hi, lo) = ((port >> 8).asInt8, (port & 0x00ff).asInt8)
        let sadr: UInt32 = self.sin_addr.s_addr.bigEndian
        let b = (Int8( sadr >> 24        ),
                 Int8((sadr >> 16) & 0xff),
                 Int8((sadr >>  8) & 0xff),
                 Int8( sadr        & 0xff))
        let z: Int8 = 0
        let data = (hi, lo, b.0, b.1, b.2, b.3, z,z,z,z,z,z,z,z)
        return sockaddr(sa_len: len, sa_family: family, sa_data: data)
    }
}

extension String {
    //絵文字など(2文字分)も含めた文字数を返します
    var length: Int {
        let string_NS = self as NSString
        return string_NS.length
    }
    
    //正規表現の検索をします
    func pregMatche(pattern: String, options: NSRegularExpression.Options = []) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: options) else {
            return false
        }
        let matches = regex.matches(in: self, options: [], range: NSMakeRange(0, self.length))
        return matches.count > 0
    }
    
    //正規表現の検索結果を利用できます
    func pregMatche(pattern: String, options: NSRegularExpression.Options = [], matches: inout [String]) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: options) else {
            return false
        }
        let targetStringRange = NSRange(location: 0, length: self.length)
        let results = regex.matches(in: self, options: [], range: targetStringRange)
        for i in 0 ..< results.count {
            for j in 0 ..< results[i].numberOfRanges {
                let range = results[i].range(at: j)
                matches.append((self as NSString).substring(with: range))
            }
        }
        return results.count > 0
    }
    
    //正規表現の置換をします
    func pregReplace(pattern: String, with: String, options: NSRegularExpression.Options = []) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: options)
        return regex.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.length), withTemplate: with)
    }
}

/*func makeCString(from str: String) -> UnsafeMutablePointer<UInt8> {
    let utf8 = str.utf8CString
    let count = utf8.count
    
    let result = UnsafeMutableBufferPointer<UInt8>.allocate(capacity: count)
    _ = utf8.withUnsafeBufferPointer { baseAddress in
        
        // 要素の中身を CChar から UInt8 にキャストする
        baseAddress.withMemoryRebound(to: UInt8.self) { baseAddress2 in
            
            // baseAddress2: UnsafeMutableBufferPointer<UInt8>
            result.initialize(from: baseAddress2)
        }
    }
    return result.baseAddress!
}*/
