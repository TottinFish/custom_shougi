//
//  State.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/18.
//  Copyright © 2018 Totty. All rights reserved.
//

import Foundation

//駒の状態保存用クラス

class State {
    struct piece {  //型情報のための構造体
        var own: Bool   //自分のか相手のか(1Pがtrue)
        let kind: Int   //駒の種類(歩兵0,香車1,桂馬2,銀将3,金将4,角行5,飛車6,王将7)
        var state: Bool //表か裏か(表がtrue)
    }
    static var boardState: [[piece?]] = [[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil]] //盤情報を保持するための二次元配列を用意
    
    static var tmpboardState: [[piece?]] = [[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil],[nil, nil, nil, nil, nil, nil, nil, nil, nil]] //盤情報を保持するための二次元配列を用意
    
    static var havingPieceNum1: [Int] = [0,0,0,0,0,0,0,0]   //持ち駒の個数保存用(1P)
    static var havingPieceNum2: [Int] = [0,0,0,0,0,0,0,0]   //持ち駒の個数保存用(2P)
    
    /*static var historyBoard: [[[piece?]]] = []  //盤面の履歴保存用
    static var historyHaving1: [[Int]] = [] //持ち駒保存用
    static var historyHaving2: [[Int]] = [] //持ち駒保存用*/
    static var history: [[Int]] = []  //履歴
    
    static var tmpPiece = piece(own: true, kind: 0, state: true)
    
    static var isMyTurn: Bool = true    //1Pが先行
    
    //とりあえず手動で設定　自分側
    static var pawn1 = piece(own: true, kind: 0, state: true)
    static var lance1 = piece(own: true, kind: 1, state: true)
    static var knight1 = piece(own: true, kind: 2, state: true)
    static var silverGeneral1 = piece(own: true, kind: 3, state: true)
    static var goldGeneral1 = piece(own: true, kind: 4, state: true)
    static var bishop1 = piece(own: true, kind: 5, state: true)
    static var rook1 = piece(own: true, kind: 6, state: true)
    static var king1 = piece(own: true, kind: 7, state: true)
    //敵側
    static var pawn2 = piece(own: false, kind: 0, state: true)
    static var lance2 = piece(own: false, kind: 1, state: true)
    static var knight2 = piece(own: false, kind: 2, state: true)
    static var silverGeneral2 = piece(own: false, kind: 3, state: true)
    static var goldGeneral2 = piece(own: false, kind: 4, state: true)
    static var bishop2 = piece(own: false, kind: 5, state: true)
    static var rook2 = piece(own: false, kind: 6, state: true)
    static var king2 = piece(own: false, kind: 7, state: true)
    
    
    //ゲーム開始時に盤情報を配列に格納するための関数
    static func initState() {
        //盤の初期化
        for x in 0..<9 {
            for y in 0..<9 {
                State.boardState[x][y] = nil
            }
        }
        let userDefaults = UserDefaults.standard
        FileController.readPieceData(isMe: true,type: userDefaults.integer(forKey: "1PPlace"))
        FileController.readPieceData(isMe: false,type: userDefaults.integer(forKey: "2PPlace"))
        //履歴初期化
        history = []
        //持ち駒初期化
        havingPieceNum1 = [0,0,0,0,0,0,0,0]
        havingPieceNum2 = [0,0,0,0,0,0,0,0]
    }
    
    //ゲーム再開時
    static func loadState() {
        FileController.loadState()
    }
    
    //受信したデータを元に駒配置
    static func setReceiveData(str: String) {
        let data = str.components(separatedBy: ",")
        print(str)
        for x in 1..<data.count - 1 {
            if(data[x] != "-1") {   //駒があるなら
                boardState[8 - (x - 1) / 4][3 - (x - 1) % 4] = State.piece(own: false, kind: Int(data[x]) ?? 0, state: true)
            }
        }
    }
    
    //デフォルトの位置
    static func defaultPlace(isMe: Bool) {
        //自分側
        if(isMe) {
            State.boardState[0][6] = pawn1
            State.boardState[1][6] = pawn1
            State.boardState[2][6] = pawn1
            State.boardState[3][6] = pawn1
            State.boardState[4][6] = pawn1
            State.boardState[5][6] = pawn1
            State.boardState[6][6] = pawn1
            State.boardState[7][6] = pawn1
            State.boardState[8][6] = pawn1
            State.boardState[1][7] = bishop1
            State.boardState[7][7] = rook1
            State.boardState[0][8] = lance1
            State.boardState[1][8] = knight1
            State.boardState[2][8] = silverGeneral1
            State.boardState[3][8] = goldGeneral1
            State.boardState[4][8] = king1
            State.boardState[5][8] = goldGeneral1
            State.boardState[6][8] = silverGeneral1
            State.boardState[7][8] = knight1
            State.boardState[8][8] = lance1
        } else {    //敵側
            State.boardState[0][2] = pawn2
            State.boardState[1][2] = pawn2
            State.boardState[2][2] = pawn2
            State.boardState[3][2] = pawn2
            State.boardState[4][2] = pawn2
            State.boardState[5][2] = pawn2
            State.boardState[6][2] = pawn2
            State.boardState[7][2] = pawn2
            State.boardState[8][2] = pawn2
            State.boardState[1][1] = rook2
            State.boardState[7][1] = bishop2
            State.boardState[0][0] = lance2
            State.boardState[1][0] = knight2
            State.boardState[2][0] = silverGeneral2
            State.boardState[3][0] = goldGeneral2
            State.boardState[4][0] = king2
            State.boardState[5][0] = goldGeneral2
            State.boardState[6][0] = silverGeneral2
            State.boardState[7][0] = knight2
            State.boardState[8][0] = lance2
        }
    }
    
}
