//
//  PageFor2Play.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//2人用画面のページそのもの
//この上に部品を乗せていく
//タッチアクションはここで指定(上の部品，つまり他のviewでこのviewにタッチアクションを指定してしまうと上書きされるので注意)

class PageFor2Play: Page {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        let miniScrWidth = screenWidth/10 //1マス分の1辺の長さ
        let miniScrHeight = (screenHeight - (miniScrWidth * 9))/2
        let reverse = CGFloat(Double.pi)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchAction))) //タッチ時のアクション(駒の移動)を指定
        firstMoveView1.image = firstMove
        firstMoveView1.frame = CGRect(x:8*miniScrWidth, y:screenHeight-miniScrHeight+miniScrWidth/10 ,width:miniScrWidth*3, height:miniScrWidth*2)
        firstMoveView2.image = firstMove
        firstMoveView2.frame = CGRect(x:-miniScrWidth, y:miniScrHeight-miniScrWidth*2,width:miniScrWidth*3, height:miniScrWidth*2)
        firstMoveView2.transform = CGAffineTransform(rotationAngle: reverse)
        passiveMoveView1.image = passiveMove
        passiveMoveView1.frame = CGRect(x:8*miniScrWidth, y:screenHeight-miniScrHeight+miniScrWidth/10 ,width:miniScrWidth*3, height:miniScrWidth*2)
        passiveMoveView2.image = passiveMove
        passiveMoveView2.frame = CGRect(x:-miniScrWidth, y:miniScrHeight-miniScrWidth*2,width:miniScrWidth*3, height:miniScrWidth*2)
        passiveMoveView2.transform = CGAffineTransform(rotationAngle: reverse)
        
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 1.0, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        
        //待ったボタン
        undoButton.frame = CGRect(x:screenWidth * 4/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        undoButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        undoButton.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        undoButton.layer.borderWidth = 1.0
        undoButton.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        undoButton.layer.cornerRadius = 25
        undoButton.layer.shadowOpacity = 0.5
        undoButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        undoButton.setTitle("待った", for: .normal)
        undoButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        undoButton.addTarget(self, action: #selector(self.undoAction(sender:)), for: .touchUpInside)
        
        //新規対局ボタン
        newButton.frame = CGRect(x:screenWidth * 7/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        newButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        newButton.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        newButton.layer.borderWidth = 1.0
        newButton.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        newButton.layer.cornerRadius = 25
        newButton.layer.shadowOpacity = 0.5
        newButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        newButton.setTitle("新規対局", for: .normal)
        newButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        newButton.addTarget(self, action: #selector(self.newAction(sender:)), for: .touchUpInside)
        
        
        
        //背景と盤の描画
        State.loadState()
        backboard = BoardImageView()
        backboard.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(backboard)
        
        //盤線
        board = PlayBoardView()
        board.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(board)
        
        //駒の描画
        makePieceView()
        
        view.addSubview(returnButton) //戻るボタン設置
        view.addSubview(undoButton) //待ったボタン設置
        view.addSubview(newButton) //新規対局ボタン設置
    
        //先手後手決定
        setFPofPlayer()
        
        //turn to offline
        Advance.isOnline = false
        
        //BGM選択
        BGMmanager.select = 1
        BGMmanager.PlaySound()
        
        Advance.root = self //このviewcontrollerを登録
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
        //様々な初期化をすべき
    }
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {//前画面に戻るAction
        self.dismiss(animated: true, completion: nil)
        BGMmanager.StopSound()
        BGMmanager.select = 3
        BGMmanager.PlaySound()
    }
    
    //待ったボタンの挙動
    @objc func undoAction(sender: UIButton) { //盤面を１つ前の状態に戻すAction
        Advance.undo()
        //再描画
        pieceView.removeFromSuperview()
        makePieceView()
        drawButtonAgain()
        //willPromoteはしない?
        if(Advance.isTouched) {
            placeView.removeFromSuperview()
            placeView = nil
            movedLastPieceView.removeFromSuperview()
            movedLastPieceView = nil
            Advance.isTouched = false
        }
    }
 
    //新規対局ボタンの挙動
    @objc func newAction(sender: UIButton) { //盤面を初期状態に戻すAction
        //盤の初期化
        for x in 0..<9 {
            for y in 0..<9 {
                State.boardState[x][y] = nil
            }
        }
        State.initState()
        Advance.ResetAdvance()  //checkmateとかの初期化
        //先手後手決定
        setFPofPlayer()
        if(placeView != nil){
            placeView.removeFromSuperview()
            placeView = nil
        }
        if(movedLastPieceView != nil){
            movedLastPieceView.removeFromSuperview()
            movedLastPieceView = nil
        }
        //再描画
        pieceView.removeFromSuperview()
        makePieceView()
        drawButtonAgain()
        //フラグのリセットは??
    }
    
    //タッチ時に呼び出されるメソッド
    @objc func touchAction(sender: UITapGestureRecognizer) {
        let CGWidth = self.view.frame.width
        let CGHeight = self.view.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        let tapPoint = sender.location(in: self.view)
        let checkmated = UIImage(named: "checkmate")
        let checkmateView:UIImageView = UIImageView(image: checkmated)
        checkmateView.frame = CGRect(x:CGWidth*5/16, y:CGHeight/3, width:CGWidth/2, height:CGHeight/2)
        let winnerMe = UIImage(named: "1Pwin")
        let winnerMeView:UIImageView = UIImageView(image: winnerMe)
        winnerMeView.frame = CGRect(x:CGWidth*3/16, y:CGHeight/3, width:CGWidth*3/4, height:CGHeight/2)
        let winnerOther = UIImage(named: "2Pwin")
        let winnerOtherView:UIImageView = UIImageView(image: winnerOther)
        winnerOtherView.frame = CGRect(x:CGWidth*3/16, y:CGHeight/3, width:CGWidth*3/4, height:CGHeight/2)
        if(Advance.willPromote) {   //成るかどうかのviewを表示しているときはタッチ無効化
            return
        }
        
        if(placeView != nil) {  //前のviewを削除(最初と範囲外指定後はnil)
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
        }
        
        //盤での位置を計算
        var touchedX = Int((tapPoint.x - sukima)/miniWidth)
        var touchedY = Int((tapPoint.y - miniHeight)/miniWidth)
        var stocktouched1 = false //自分側の持ち駒がタッチされた時に使う
        var stocktouched2 = false //敵側の持ち駒がタッチされた時に使う
        
        if((tapPoint.x - sukima) < 0 ) {
            touchedX = -1
        }else if(miniHeight > tapPoint.y && tapPoint.y > miniHeight - sukima ){
            touchedY = -1
        }else if(miniHeight - sukima - miniWidth < tapPoint.y && tapPoint.y < miniHeight - sukima){
            stocktouched2 = true
            if(touchedY == 0){
                touchedY = -1
            }
        }else if(touchedY == 9 || touchedY == 10){
            stocktouched1 = true
        }
        
        //駒を移動
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && Advance.isAbleToMove(i: touchedX, j: touchedY)) {
            Advance.move(i: touchedX, j: touchedY)  //駒の移動(データ)
            //再描画
            pieceView.removeFromSuperview()
            makePieceView()
            if(movedLastPieceView != nil){
                movedLastPieceView.removeFromSuperview()
            }
            makeLastMoveView()
            Advance.isTouched = false    //状態変更
            Advance.makePromoteView()   //成るかどうかを選択するviewを表示
            drawButtonAgain()
            
            if(Advance.isCheckmatedMe){
                Advance.judgeEnd(isMe: true)
            }
            if(Advance.isCheckmatedOther){
                Advance.judgeEnd(isMe: false)
            }
            
            if((Advance.isCheckmatedMe || Advance.isCheckmatedOther) && (!Advance.isWinnerMe && !Advance.isWinnerOther)){//王手の表示
                self.view.addSubview(checkmateView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    // 1.5秒後に実行したい処理
                    checkmateView.removeFromSuperview()
                }
            }
            if(Advance.isWinnerMe){//勝者の表示
                self.view.addSubview(winnerMeView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    // 5.0秒後に実行したい処理
                    winnerMeView.removeFromSuperview()
                }
            }else if(Advance.isWinnerOther){//勝者の表示
                self.view.addSubview(winnerOtherView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    // 5.0秒後に実行したい処理
                    winnerOtherView.removeFromSuperview()
                }
            }
            return
        }
        
        //移動可能場所を表示するview
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && State.boardState[touchedX][touchedY] != nil){    //駒がある時
            Advance.isTouched = true    //状態変更
            Advance.placeCandidate(i: touchedX, j: touchedY, touch: true, roop: false)  //移動可能場所を探す(placeに保存される)
            makePlaceView()
            //敵の駒の持ち駒配置
        } else if(0 <= touchedX && touchedX <= 8 && stocktouched2 ) {
            //相手の持ち駒配置
            switch touchedX{
            case 8:
                if(State.havingPieceNum2[0] != 0) {
                    SetStock2(KindOfPiece: 0)
                }
            case 7:
                if(State.havingPieceNum2[1] != 0) {
                    SetStock2(KindOfPiece: 1)
                }
            case 6:
                if(State.havingPieceNum2[2] != 0) {
                    SetStock2(KindOfPiece: 2)
                }
            case 5:
                if(State.havingPieceNum2[3] != 0) {
                    SetStock2(KindOfPiece: 3)
                }
            case 4:
                if(State.havingPieceNum2[4] != 0) {
                    SetStock2(KindOfPiece: 4)
                }
            case 3:
                if(State.havingPieceNum2[5] != 0) {
                    SetStock2(KindOfPiece: 5)
                }
            case 2:
                if(State.havingPieceNum2[6] != 0) {
                    SetStock2(KindOfPiece: 6)
                }
            case 1:
                if(State.havingPieceNum2[7] != 0) {
                    SetStock2(KindOfPiece: 7)
                }
            default:
                removeAllSubviews(parentView: placeView)
                placeView.removeFromSuperview()
                placeView = nil
                Advance.isTouched = false
            }
        }else if(0 <= touchedX && touchedX <= 8 && stocktouched1 ) {
            //自分の持ち駒配置
            switch touchedX{
            case 0:
                if(State.havingPieceNum1[0] != 0) {
                    SetStock1(KindOfPiece: 0)
                }
            case 1:
                if(State.havingPieceNum1[1] != 0) {
                    SetStock1(KindOfPiece: 1)
                }
            case 2:
                if(State.havingPieceNum1[2] != 0) {
                    SetStock1(KindOfPiece: 2)
                }
            case 3:
                if(State.havingPieceNum1[3] != 0) {
                    SetStock1(KindOfPiece: 3)
                }
            case 4:
                if(State.havingPieceNum1[4] != 0) {
                    SetStock1(KindOfPiece: 4)
                }
            case 5:
                if(State.havingPieceNum1[5] != 0) {
                    SetStock1(KindOfPiece: 5)
                }
            case 6:
                if(State.havingPieceNum1[6] != 0) {
                    SetStock1(KindOfPiece: 6)
                }
            case 7:
                if(State.havingPieceNum1[7] != 0) {
                    SetStock1(KindOfPiece: 7)
                }
            default:
                removeAllSubviews(parentView: placeView)
                placeView.removeFromSuperview()
                placeView = nil
                Advance.isTouched = false
            }
        } else if(placeView != nil) {   //範囲外指定を連続でした時は既にnilなので
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
            placeView = nil
            Advance.isTouched = false    //状態変更
        }
        
        drawButtonAgain()
    }
    
    //subviewを全て削除
    func removeAllSubviews(parentView: UIView){
        let subviews = parentView.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func makePlaceView() {
        placeView = MovePlaceView()
        placeView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        placeView.backgroundColor = UIColor.clear
        placeView.alpha = 0.5
        self.view.addSubview(placeView)
    }
    
    override func makePieceView() {
        pieceView = PieceView()
        pieceView.setInit(playView: self)
        pieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        pieceView.backgroundColor = UIColor.clear
        view.addSubview(pieceView)
    }
    
    func makeLastMoveView() {
        movedLastPieceView = LastMovedPiece()
        movedLastPieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        movedLastPieceView.backgroundColor = UIColor.clear
        movedLastPieceView.alpha = 0.35
        self.view.addSubview(movedLastPieceView)
    }
    
    func SetStock1(KindOfPiece:Int){//1Pの持ち駒配置の描写
        Advance.isTouched = true    //状態変更
        Advance.isAbleToPut(kind: KindOfPiece, isMe: true)   //置ける場所を調べる(placeに保存される)
        makePlaceView()
    }
    
    func SetStock2(KindOfPiece:Int){//2Pの持ち駒配置の描写
        Advance.isTouched = true    //状態変更
        Advance.isAbleToPut(kind: KindOfPiece, isMe: false)   //置ける場所を調べる(placeに保存される)
        makePlaceView()
    }
    
    func drawButtonAgain() {
        returnButton.removeFromSuperview()
        undoButton.removeFromSuperview()
        newButton.removeFromSuperview()
        view.addSubview(returnButton) //戻るボタン設置
        view.addSubview(undoButton) //待ったボタン設置
        view.addSubview(newButton) //新規対局ボタン設置
    }
    
    
    func setFPofPlayer(){   //set First Passive of Player
        for _ in 0 ... (arc4random()%2) {//0か1のランダム生成
            State.isMyTurn = !State.isMyTurn//先行後攻変更
        }
        Advance.is1Pfirst = State.isMyTurn //1Pの先行後攻同期
        firstMoveView1.removeFromSuperview()
        passiveMoveView1.removeFromSuperview()
        firstMoveView2.removeFromSuperview()
        passiveMoveView2.removeFromSuperview()
        if(State.isMyTurn){
            self.view.addSubview(firstMoveView1)
            self.view.addSubview(passiveMoveView2)
        }else{
            self.view.addSubview(firstMoveView2)
            self.view.addSubview(passiveMoveView1)
        }
    }
    
}



