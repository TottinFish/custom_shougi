//
//  TitleBackView.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/23.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class TitleBackView: UIView {
    
    override func draw(_ rect: CGRect){
        let screenWidth = self.frame.width
        let screenHeight = self.frame.height
        let backImage = UIImage(named: "newyear2")
        let backImageView = UIImageView(image: backImage)
        
        backImageView.frame = CGRect(x:0, y:0, width:screenWidth, height:screenHeight)
        self.addSubview(backImageView)
    }
}
