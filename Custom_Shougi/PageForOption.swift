//
//  PageForOption.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

class PageForOption: UIViewController,UITextFieldDelegate {
    let configs = UserDefaults.standard //保存データ
    let returnButton = UIButton()   //戻るボタン
    let userNameLabel = UITextView()    //ユーザ名ラベル
    let userNameField = UITextField()   //ユーザ名入力フィールド
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        
        //タイトルラベル(後から消す)
        /*titleLabel.text = "(option)"
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HiraKakuProN-W3", size:50)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.3
        titleLabel.frame = CGRect(x:screenWidth/4, y:screenHeight * 4/20 ,width:screenWidth/2, height:screenHeight * 1/10)
        titleLabel.textAlignment = NSTextAlignment.center
        view.addSubview(titleLabel)*/
        
        //ユーザ名のラベル
        userNameLabel.text = "ユーザ名"
        userNameLabel.frame = CGRect(x:screenWidth/4, y: screenHeight * 5/40 ,width:screenWidth/2, height: screenHeight * 3/40)
        userNameLabel.textAlignment = NSTextAlignment.center
        userNameLabel.font = UIFont.systemFont(ofSize: 23)
        view.addSubview(userNameLabel)
        
        //ユーザ名の入力フィールド
        userNameField.frame = CGRect(x:screenWidth/4, y: screenHeight * 8/40, width:screenWidth/2, height: screenHeight * 2/40 )
        userNameField.text = userDefaults.object(forKey: "userName") as? String
        userNameField.delegate = self
        userNameField.tag = 0
        userNameField.borderStyle = .roundedRect    //枠を表示
        userNameField.leftViewMode = .always    //文字が左にひっつかないための余白を追加
        userNameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))//左側の余白
        view.addSubview(userNameField)
        
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/8, y:screenHeight * 9/10, width:screenWidth/4, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 0.3, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 0.3, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        view.addSubview(returnButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
        
    }
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {
        if(userNameField.text != "") {  //空欄でなければ保存
            userDefaults.set(userNameField.text, forKey: "userName")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // キーボードを閉じるためのメソッド
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
