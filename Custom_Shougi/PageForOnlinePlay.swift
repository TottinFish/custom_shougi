//
//  PageForOnlinePlay.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2018/12/16.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//オンライン対戦用画面

class PageForOnlinePlay: Page {
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenWidth = self.view.frame.width
        let screenHeight:CGFloat = self.view.frame.height
        let miniScrWidth = screenWidth/10 //1マス分の1辺の長さ
        let miniScrHeight = (screenHeight - (miniScrWidth * 9))/2
        let reverse = CGFloat(Double.pi)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchAction))) //タッチ時のアクション(駒の移動)を指定
        firstMoveView1.image = firstMove
        firstMoveView1.frame = CGRect(x:8*miniScrWidth, y:screenHeight-miniScrHeight+miniScrWidth/10 ,width:miniScrWidth*3, height:miniScrWidth*2)
        firstMoveView2.image = firstMove
        firstMoveView2.frame = CGRect(x:-miniScrWidth, y:miniScrHeight-miniScrWidth*2,width:miniScrWidth*3, height:miniScrWidth*2)
        firstMoveView2.transform = CGAffineTransform(rotationAngle: reverse)
        passiveMoveView1.image = passiveMove
        passiveMoveView1.frame = CGRect(x:8*miniScrWidth, y:screenHeight-miniScrHeight+miniScrWidth/10 ,width:miniScrWidth*3, height:miniScrWidth*2)
        passiveMoveView2.image = passiveMove
        passiveMoveView2.frame = CGRect(x:-miniScrWidth, y:miniScrHeight-miniScrWidth*2,width:miniScrWidth*3, height:miniScrWidth*2)
        passiveMoveView2.transform = CGAffineTransform(rotationAngle: reverse)
        
        //戻るボタン
        returnButton.frame = CGRect(x:screenWidth/10, y:screenHeight * 1/20, width:screenWidth/5, height: screenHeight * 2/40)
        returnButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        returnButton.backgroundColor = UIColor(red: 1.0, green: 0.7, blue: 0.6, alpha: 1)
        returnButton.layer.borderWidth = 1.0
        returnButton.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.5, alpha: 1).cgColor
        returnButton.layer.cornerRadius = 25
        returnButton.layer.shadowOpacity = 0.5
        returnButton.layer.shadowOffset = CGSize(width: 2, height: 2)
        returnButton.setTitle("戻る", for: .normal)
        returnButton.setTitleColor(UIColor.red, for: UIControl.State.highlighted)
        returnButton.addTarget(self, action: #selector(self.returnAction(sender:)), for: .touchUpInside)
        
        Advance.ResetAdvance()  //checkmateとかの初期化
        
        //背景と盤の描画
        backboard = BoardImageView()
        backboard.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(backboard)
        
        //盤線
        board = PlayBoardView()
        board.frame = CGRect(x:0, y:0, width:screenWidth, height: screenHeight)
        view.addSubview(board)
        
        //駒の描画
        makePieceView()
        
        view.addSubview(returnButton) //戻るボタン設置
        
        //turn to online
        Advance.isOnline = true
        
        //for test
        if(!Advance.AmI1P){
            State.isMyTurn = false
        }
        
        //1Pがは先手かどうか同期
        Advance.is1Pfirst = State.isMyTurn
        
        //BGM選択
        BGMmanager.select = 1
        BGMmanager.PlaySound()
        
        Advance.root = self //このviewcontrollerを登録
        
        let queue = OperationQueue()
        queue.addOperation() {
            while(isConnecting) {
                print("こっち")
                let receiveData = Transmission().listenAndRead(rootView: self)
                if(!isConnecting) {
                    print("オンラインで")
                    return
                } else {
                    OperationQueue.main.addOperation() {
                        //ここでview更新
                        Advance.receiveMove(receiveData: receiveData)
                        self.pieceView.removeFromSuperview()
                        self.makePieceView()
                        Advance.isTouched = false    //状態変更
                        self.drawButtonAgain()
                    }
                }
            }
         }
    }
    
    override func viewWillAppear(_ animated: Bool) {  //再読み込み
    }
    
    //戻るボタンの挙動
    @objc func returnAction(sender: UIButton) {
        BGMmanager.StopSound()
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //タッチ時に呼び出されるメソッド
    @objc func touchAction(sender: UITapGestureRecognizer) {
        let CGWidth = self.view.frame.width
        let CGHeight = self.view.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        let tapPoint = sender.location(in: self.view)
        let checkmated = UIImage(named: "checkmate")
        let checkmateView:UIImageView = UIImageView(image: checkmated)
        checkmateView.frame = CGRect(x:CGWidth*5/16, y:CGHeight/3, width:CGWidth/2, height:CGHeight/2)
        let winnerMe = UIImage(named: "1Pwin")
        let winnerMeView:UIImageView = UIImageView(image: winnerMe)
        winnerMeView.frame = CGRect(x:CGWidth*3/16, y:CGHeight/3, width:CGWidth*3/4, height:CGHeight/2)
        let winnerOther = UIImage(named: "2Pwin")
        let winnerOtherView:UIImageView = UIImageView(image: winnerOther)
        winnerOtherView.frame = CGRect(x:CGWidth*3/16, y:CGHeight/3, width:CGWidth*3/4, height:CGHeight/2)
        if(Advance.willPromote) {   //成るかどうかのviewを表示しているときはタッチ無効化
            return
        }
        
        if(placeView != nil) {  //前のviewを削除(最初と範囲外指定後はnil)
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
        }
        
        //盤での位置を計算
        var touchedX = Int((tapPoint.x - sukima)/miniWidth)
        var touchedY = Int((tapPoint.y - miniHeight)/miniWidth)
        var stocktouched1 = false //自分側の持ち駒がタッチされた時に使う
        var stocktouched2 = false //敵側の持ち駒がタッチされた時に使う
        
        if((tapPoint.x - sukima) < 0 ) {
            touchedX = -1
        }else if(miniHeight > tapPoint.y && tapPoint.y > miniHeight - sukima ){
            touchedY = -1
        }else if(miniHeight - sukima - miniWidth < tapPoint.y && tapPoint.y < miniHeight - sukima){
            stocktouched2 = true
            if(touchedY == 0){
                touchedY = -1
            }
        }else if(touchedY == 9 || touchedY == 10){
            stocktouched1 = true
        }
        
        //駒を移動
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && Advance.isAbleToMove(i: touchedX, j: touchedY)) {
            Advance.move(i: touchedX, j: touchedY)  //駒の移動(データ)
            //再描画
            pieceView.removeFromSuperview()
            makePieceView()
            if(movedLastPieceView != nil){
                movedLastPieceView.removeFromSuperview()
            }
            makeLastMoveView()
            Advance.isTouched = false    //状態変更
            Advance.makePromoteView()   //成るかどうかを選択するviewを表示
            drawButtonAgain()
            
            if(Advance.isCheckmatedMe){
                Advance.judgeEnd(isMe: true)
            }
            if(Advance.isCheckmatedOther){
                Advance.judgeEnd(isMe: false)
            }
            
            if((Advance.isCheckmatedMe || Advance.isCheckmatedOther) && (!Advance.isWinnerMe && !Advance.isWinnerOther)){//王手の表示
                self.view.addSubview(checkmateView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    // 1.5秒後に実行したい処理
                    checkmateView.removeFromSuperview()
                }
            }
            if(Advance.isWinnerMe){//勝者の表示
                self.view.addSubview(winnerMeView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    // 5.0秒後に実行したい処理
                    winnerMeView.removeFromSuperview()
                }
            }else if(Advance.isWinnerOther){//勝者の表示
                self.view.addSubview(winnerOtherView)
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    // 5.0秒後に実行したい処理
                    winnerOtherView.removeFromSuperview()
                }
            }
            return
        }
        
        //移動可能場所を表示するview
        if(0 <= touchedX && touchedX <= 8 && 0 <= touchedY && touchedY <= 8 && State.boardState[touchedX][touchedY] != nil){    //駒がある時
            Advance.isTouched = true    //状態変更
            Advance.placeCandidate(i: touchedX, j: touchedY, touch: true, roop: false)  //移動可能場所を探す(placeに保存される)
            makePlaceView()
            //敵の駒の持ち駒配置
        } else if(0 <= touchedX && touchedX <= 8 && stocktouched2 ) {
            //相手の持ち駒配置
            switch touchedX{
            case 8:
                if(State.havingPieceNum2[0] != 0) {
                    SetStock2(KindOfPiece: 0)
                }
            case 7:
                if(State.havingPieceNum2[1] != 0) {
                    SetStock2(KindOfPiece: 1)
                }
            case 6:
                if(State.havingPieceNum2[2] != 0) {
                    SetStock2(KindOfPiece: 2)
                }
            case 5:
                if(State.havingPieceNum2[3] != 0) {
                    SetStock2(KindOfPiece: 3)
                }
            case 4:
                if(State.havingPieceNum2[4] != 0) {
                    SetStock2(KindOfPiece: 4)
                }
            case 3:
                if(State.havingPieceNum2[5] != 0) {
                    SetStock2(KindOfPiece: 5)
                }
            case 2:
                if(State.havingPieceNum2[6] != 0) {
                    SetStock2(KindOfPiece: 6)
                }
            case 1:
                if(State.havingPieceNum2[7] != 0) {
                    SetStock2(KindOfPiece: 7)
                }
            default:
                removeAllSubviews(parentView: placeView)
                placeView.removeFromSuperview()
                placeView = nil
                Advance.isTouched = false
            }
        }else if(0 <= touchedX && touchedX <= 8 && stocktouched1 ) {
            //自分の持ち駒配置
            switch touchedX{
            case 0:
                if(State.havingPieceNum1[0] != 0) {
                    SetStock1(KindOfPiece: 0)
                }
            case 1:
                if(State.havingPieceNum1[1] != 0) {
                    SetStock1(KindOfPiece: 1)
                }
            case 2:
                if(State.havingPieceNum1[2] != 0) {
                    SetStock1(KindOfPiece: 2)
                }
            case 3:
                if(State.havingPieceNum1[3] != 0) {
                    SetStock1(KindOfPiece: 3)
                }
            case 4:
                if(State.havingPieceNum1[4] != 0) {
                    SetStock1(KindOfPiece: 4)
                }
            case 5:
                if(State.havingPieceNum1[5] != 0) {
                    SetStock1(KindOfPiece: 5)
                }
            case 6:
                if(State.havingPieceNum1[6] != 0) {
                    SetStock1(KindOfPiece: 6)
                }
            case 7:
                if(State.havingPieceNum1[7] != 0) {
                    SetStock1(KindOfPiece: 7)
                }
            default:
                removeAllSubviews(parentView: placeView)
                placeView.removeFromSuperview()
                placeView = nil
                Advance.isTouched = false
            }
        } else if(placeView != nil) {   //範囲外指定を連続でした時は既にnilなので
            removeAllSubviews(parentView: placeView)
            placeView.removeFromSuperview()
            placeView = nil
            Advance.isTouched = false    //状態変更
        }
        
        drawButtonAgain()
    }
    
    //subviewを全て削除
    func removeAllSubviews(parentView: UIView){
        let subviews = parentView.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    func makePlaceView() {
        placeView = MovePlaceView()
        placeView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        placeView.backgroundColor = UIColor.clear
        placeView.alpha = 0.5
        self.view.addSubview(placeView)
    }
    
    override func makePieceView() {
        pieceView = PieceView()
        pieceView.setInit(playView: self)
        pieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        pieceView.backgroundColor = UIColor.clear
        view.addSubview(pieceView)
    }
    
    func makeLastMoveView() {
        movedLastPieceView = LastMovedPiece()
        movedLastPieceView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height: self.view.frame.height)
        movedLastPieceView.backgroundColor = UIColor.clear
        movedLastPieceView.alpha = 0.35
        self.view.addSubview(movedLastPieceView)
    }
    
    func SetStock1(KindOfPiece:Int){//1Pの持ち駒配置の描写
        Advance.isTouched = true    //状態変更
        Advance.isAbleToPut(kind: KindOfPiece, isMe: true)   //置ける場所を調べる(placeに保存される)
        makePlaceView()
    }
    
    func SetStock2(KindOfPiece:Int){//2Pの持ち駒配置の描写
        Advance.isTouched = true    //状態変更
        Advance.isAbleToPut(kind: KindOfPiece, isMe: false)   //置ける場所を調べる(placeに保存される)
        makePlaceView()
    }
    
    func drawButtonAgain() {
        returnButton.removeFromSuperview()
        view.addSubview(returnButton) //戻るボタン設置
    }
    
    
    func setFPofPlayer(){   //set First Passive of Player
        for _ in 0 ... (arc4random()%2) {//0か1のランダム生成
            State.isMyTurn = !State.isMyTurn//先行後攻変更
        }
        firstMoveView1.removeFromSuperview()
        passiveMoveView1.removeFromSuperview()
        firstMoveView2.removeFromSuperview()
        passiveMoveView2.removeFromSuperview()
        if(State.isMyTurn){
            self.view.addSubview(firstMoveView1)
            self.view.addSubview(passiveMoveView2)
        }else{
            self.view.addSubview(firstMoveView2)
            self.view.addSubview(passiveMoveView1)
        }
    }
    
}



