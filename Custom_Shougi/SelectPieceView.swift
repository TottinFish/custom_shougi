//
//  SelectPieceView.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/13.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit

class SelectPieceView: UIView {
    
    var touchView: UIViewController!
    var pieceCount:Int = 0
    static var piecePage:Int = 0
    var pieceX:CGFloat = 0
    var pieceY:CGFloat = 0
    var skip: Int = 0
    
    func setInit(touchView: UIViewController) { //持ち駒タップ時に上のviewに伝えるため保存
        self.touchView = touchView
    }
    
    override func draw(_ rect: CGRect){
        let screenWidth = self.frame.width
        let screenHeight = self.frame.height
        let oneBlock:CGFloat = screenWidth/4
        var imageView: UIImageView
        var trueCount: Int = 0
        
        pieceCount = 0
        pieceX = 0
        pieceY = 0
        skip = SelectPieceView.piecePage * 20
        skip += adjustCount(setNum: skip)
        
        for piece in UserInfo.PieceBool {
            if(skip > 0){
                skip -= 1
                pieceCount += 1
                continue
            }
            if(piece){
                imageView = UIImageView(image: ImageData.NoAdv[pieceCount][0])
                // TODO: 画像の位置配置について
                imageView.frame = CGRect(x:oneBlock * pieceX, y:screenHeight/6 + (oneBlock * pieceY), width:oneBlock, height:oneBlock)
                /*imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: touchView, action: #selector(PageForPieceSelect.touchAction)))*/
                self.addSubview(imageView)
                pieceX += 1
                if(pieceX == 4){
                    pieceX = 0
                    pieceY += 1
                }
                if(pieceY == 5){
                    pieceY = 0
                }
                trueCount += 1
            }
            pieceCount += 1
            if(trueCount%20 == 0){
                break
            }
        }
    }
    
    func adjustCount(setNum: Int)->Int{
        var adjC = 0
        
        for x in 0..<setNum{
            if(!UserInfo.PieceBool[x]){
                adjC += 1
            }
        }
        
        return adjC
    }
    
}
