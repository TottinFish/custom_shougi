//
//  MovePlaceView.swift
//  Custom_Shougi
//
//  Created by 角田有紀子 on 2018/12/20.
//  Copyright © 2018 Totty. All rights reserved.
//

import UIKit

//駒の移動可能場所を描画

class MovePlaceView: UIView {
    //移動可能場所に印
    override func draw(_ rect: CGRect){
        let CGWidth = self.frame.width
        let CGHeight = self.frame.height
        let miniWidth = CGWidth/10 //1マス分の1辺の長さ
        let miniHeight = (CGHeight - (miniWidth * 9))/2
        let sukima = CGWidth/20
        
        for i in 0..<9 {
            for j in 0..<9 {
                let PositionX = (CGFloat((i + 1/2)) * miniWidth) + sukima
                let PositionY = (CGFloat((j + 1/2)) * miniWidth) + miniHeight
                
                if(Advance.place[i][j]) {
                    var myCircle = UIBezierPath()   //グラフの点
                    let circlePoint = CGPoint(x:PositionX + miniWidth/2, y:PositionY + miniWidth/2)
                    myCircle = UIBezierPath(arcCenter: circlePoint,radius: miniWidth/2,startAngle: 0.0,endAngle: CGFloat(Double.pi*2),/*反時計回り*/clockwise: false)
                    UIColor.white.setFill()
                    myCircle.fill()
                    myCircle.stroke()
                }
            }
        }
    }

}
