//
//  BGMmanager.swift
//  Custom_Shougi
//
//  Created by 田中稔也 on 2019/03/02.
//  Copyright © 2019 Totty. All rights reserved.
//

import UIKit
import AVFoundation

class BGMmanager {
    
    static var player: AVAudioPlayer?//BGM用
    static let bgmData : [NSDataAsset] = [NSDataAsset(name: "DBD fileNo3")!,NSDataAsset(name: "wood note")!,NSDataAsset(name: "The 4th Dimension")!, NSDataAsset(name: "kirameki")!]
    static var select: Int = 0
    
    static func PlaySound() {
            player = try? AVAudioPlayer(data: bgmData[select].data)
            player?.numberOfLoops = -1
            player?.play() // → これで音が鳴る
    }
    
    static func StopSound(){
        BGMmanager.player?.stop()
    }
    
    static func PauseSound(){
        BGMmanager.player?.pause()
    }
    
}
