<?php
    
    $socketHole = "tcp://133.167.88.52:1048";
    $errorNunmber = null;
    $errorMessage = null;
    $endLog = "($)end($)";
    $hogeLog = "xxx";
    $socket = stream_socket_server($socketHole, $errorNunmber, $errorMessage);
    $timeCount = 0;
    if ($socket === false) {
        print("ソケットの確立に失敗しました。");
        exit();
    } else {
        // このソケットサーバーに接続してきた全クライアントを監視用の配列に確保する
        $wrapper = array(); //クライアント
        $pair = array();    //対戦相手
        $state = array();   //今の進行状態(1が接続要求来たけど名前まだ、2が名前登録してcli候補、3がsrv候補、4がペア成立、5が終了待ち)
        $name = array();    //名前とランクを含む文字列
        $key = array();    //番号
        
        while(true) {
            $stream = @stream_socket_accept($socket, 0.3);
            if ($stream !== false) {
                // クライアントの接続時
                $clientName = stream_socket_get_name($stream, true);
                print($clientName.PHP_EOL);
                // ストリーム監視用の配列に確保
                $wrapper[$clientName] = $stream;
                //state保存(接続要求来たけど名前まだ)
                $state[$clientName] = 1;
                
                /*if((count($wrapper) - count($wait)) % 2 === 0) {    //とりあえず来た順にペアにしていく
                    print("pair".PHP_EOL);
                    //相手をお互い登録
                    $pair[$clientName] = $before;
                    $pair[$before] = $clientName;
                    //ペア成立をクライアントへ報告
                    $sendingLog = "1P";
                    fwrite($wrapper[$before], $sendingLog, strlen($sendingLog));
                    $sendingLog = "2P";
                    fwrite($wrapper[$clientName], $sendingLog, strlen($sendingLog));
                } else {    //1人目
                    print("one".PHP_EOL);
                    $before = $clientName;
                }*/
            }
            // クライアントが0の状態を予防
            if (count($wrapper) === 0) {
                continue;
            }

            //クライアントが落ちないように定期的に文字送信
            $timeCount = $timeCount + 1;
            $timeCount = $timeCount % 7;
            if($timeCount === 0 || $timeCount === 4) {
                $nameList = "";
                foreach ($wrapper as $innerKey => $innerFp) {
                    if($state[$innerKey] === 3) {
                        $nameList = $nameList . $name[$innerKey] . $key[$innerKey] . "($$)";
                    }
                }
                foreach ($wrapper as $innerKey => $innerFp) {
                    if($state[$innerKey] === 2) {
                        $nameF = "($)" . $nameList . "($)";
                        fwrite($innerFp, $nameF, strlen($nameF));
                    } else if($timeCount === 0 && $state[$innerKey] === 3 || $state[$innerKey] === 4) {    //1P候補かペア成立
                        if(!fwrite($innerFp, $hogeLog, strlen($hogeLog)) && $state[$innerKey] === 4 && !empty($state[$pair[$innerKey]])) {  //駒情報交換直後に片方切断
                            $state[$pair[$innerKey]] = 5;
                            fwrite($wrapper[$pair[$innerKey]], $endLog, strlen($endLog)); //対戦相手へ報告
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                            unset($name[$readClientName]);
                            unset($key[$readClientName]);
                            unset($pair[$readClientName]);
                        }
                        //fwrite($innerFp, $hogeLog, strlen($hogeLog));
                    }
                }
            }
            // 接続されたソケットを管理
            $read = $write = $error = $wrapper;
            $number = stream_select($read, $write, $error, 0);
            if ($number > 0) {
                foreach ($read as $i => $fp) {
                    $temp = fread($fp, 1024);
                    $readClientName = stream_socket_get_name($fp, true);
                    
                    if(empty($state[$readClientName])) {
                        if (strlen($temp) === 0) {  //切断
                        } else {
                            print("[$readClientName]close!!!");
                        }
                        continue;
                    }
                    if($state[$readClientName] === 1) {  //接続要求来たけど名前まだ
                        if (strlen($temp) === 0) {  //切断
                            print("closed1:[$readClientName]".PHP_EOL);
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                        } else if(0 === strpos($temp, "custom")) {
                            $state[$readClientName] = 2;
                            $name[$readClientName] = substr($temp, 6, strlen($temp) - 6);  //名前やランクを保存
                            $keyTmp = mt_rand(1,100000000);  //ユーザ特定用の番号を振る
                            while(in_array($keyTmp, $key)) {
                                $keyTmp = mt_rand(1,100000000);
                            }
                            $key[$readClientName] = $keyTmp;
                        } else {
                        }
                    } else if($state[$readClientName] === 2) {  //cli候補
                        if (strlen($temp) === 0) {  //切断
                            print("closed2:[$readClientName]".PHP_EOL);
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                            unset($name[$readClientName]);
                            unset($key[$readClientName]);
                        } else if($temp == "srv") {  //サーバーになる
                            $state[$readClientName] = 3;
                        } else if(strlen($temp) === 0) {
                        } else {    //相手を指定したため、ペア作成
                            $pairTmp = array_search((int)$temp, $key);
                            if(!empty($state[$pairTmp]) && $state[$pairTmp] === 3) {
                                $pair[$readClientName] = $pairTmp;
                                $pair[$pairTmp] = $readClientName;
                                $state[$readClientName] = 4;
                                $state[$pairTmp] = 4;
                                if(rand(1,9) % 2 === 0) {
                                    $sendTmp1 = "($)1P($)" . $name[$readClientName];
                                    $sendTmp2 = "($)2P($)";
                                    fwrite($wrapper[$pairTmp], $sendTmp1, strlen($sendTmp1));
                                    fwrite($wrapper[$readClientName], $sendTmp2, strlen($sendTmp2));
                                } else {
                                    $sendTmp1 = "($)2P($)" . $name[$readClientName];
                                    $sendTmp2 = "($)1P($)";
                                    fwrite($wrapper[$pairTmp], $sendTmp1, strlen($sendTmp1));
                                    fwrite($wrapper[$readClientName], $sendTmp2, strlen($sendTmp2));
                                }
                            } else {
                                $sendTmp = "($)false($)";
                            }
                        }
                    } else if($state[$readClientName] === 3) {
                        if (strlen($temp) === 0) {  //切断
                            print("closed3:[$readClientName]".PHP_EOL);
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                            unset($name[$readClientName]);
                            unset($key[$readClientName]);
                        } else {
                            
                        }
                    } else if($state[$readClientName] === 4) {
                        if (strlen($temp) === 0) {  //切断
                            print("closed4:[$readClientName]".PHP_EOL);
                            fwrite($wrapper[$pair[$readClientName]], $endLog, strlen($endLog)); //対戦相手へ報告
                            $state[$pair[$readClientName]] = 5;
                            //fclose($wrapper[$readClientName]);
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                            unset($name[$readClientName]);
                            unset($key[$readClientName]);
                            unset($pair[$readClientName]);
                        } else {
                            //対戦相手へ送信
                            print("[$readClientName]:[$temp]".PHP_EOL);
                            $sendingLog = "(\$)$temp(\$)";
                            $fwrite = fwrite($wrapper[$pair[$readClientName]], $sendingLog, strlen($sendingLog));
                            /*if ($fwrite === false and !empty($wrapper[$readClientName])) {
                             fwrite($wrapper[$readClientName], $endLog, strlen($endLog)); //対戦相手が切断したことを連絡(コネクト中とか)
                             }*/
                        }
                    } else {    //切断待ち
                        if (strlen($temp) === 0) {  //切断
                            print("closed5:[$readClientName]".PHP_EOL);
                            unset($wrapper[$readClientName]);
                            unset($state[$readClientName]);
                            unset($name[$readClientName]);
                            unset($key[$readClientName]);
                            unset($pair[$readClientName]);
                        } else {
                            
                        }
                    }
                    
                    
                    /*if(!empty($wait[$readClientName])) {
                        if (strlen($temp) === 0) {    //もう片方も閉じた
                            print("closed2:[$readClientName]".PHP_EOL);
                            unset($wait[$readClientName]);
                            fclose($wrapper[$readClientName]);
                            unset($wrapper[$readClientName]);
                            unset($pair[$readClientName]);
                        } else {
                            print("close!!!");
                            fwrite($wrapper[$readClientName], $endLog, strlen($endLog)); //閉じろと連絡
                        }
                        continue;
                    }
                    if(empty($wrapper[$readClientName])) {  //削除されているのに受信(クライアントがendを見ずに送信し続けてる)
                        print("aaa".PHP_EOL);
                        continue;
                    }
                    if(empty($pair[$readClientName])) {  //ペアがまだいないクライアントからの受信(中断か違法)
                        fclose($wrapper[$readClientName]);
                        unset($wrapper[$readClientName]);
                        print("closed:[$readClientName]".PHP_EOL);
                        continue;
                    }
                    if(empty($wrapper[$pair[$readClientName]])) {
                        print("xxx".PHP_EOL);
                        continue;
                    }
                    
                    if (strlen($temp) === 0) {  //クライアントがソケットを閉じた
                        print("closed1:[$readClientName]".PHP_EOL);
                        fwrite($wrapper[$pair[$readClientName]], $endLog, strlen($endLog)); //対戦相手へ報告
                        fclose($wrapper[$readClientName]);
                        $wait[$pair[$readClientName]] = 1;
                        unset($wrapper[$readClientName]);
                        unset($pair[$readClientName]);
                        continue;
                    }
                    //対戦相手へ送信
                    print("[$readClientName]:[$temp]".PHP_EOL);
                    $sendingLog = "[$temp]";
                    $fwrite = fwrite($wrapper[$pair[$readClientName]], $sendingLog, strlen($sendingLog));
                    if ($fwrite === false and !empty($wrapper[$readClientName])) {
                        fwrite($wrapper[$readClientName], $endLog, strlen($endLog)); //対戦相手が切断したことを連絡(コネクト中とか)
                    }*/
                }
            }
        }
    }
    fclose($socket);
